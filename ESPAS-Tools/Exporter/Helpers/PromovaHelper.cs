﻿using System.Collections.Generic;
using System.Linq;
using DataDomain.DataObjects;
using DataDomain.Utils;

namespace Exporter.Helpers
{
    public class PromovaHelper
    {
        public static IDictionary<string, PersonData> GetPersons(ServerDispatcher dispatcher, ClientAppState clientAppState)
        {
            var persons = dispatcher.PersonBusinessLogic.GetPersons(clientAppState)
                .ToArray()
                .Where(x => x.Deleted == false)
                //.Take(100) // Just for testing
                .ToDictionary(x => x.ID, y => y);
            return persons;
        }
    }
}
