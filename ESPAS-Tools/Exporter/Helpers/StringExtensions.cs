﻿namespace Exporter.Helpers
{
    public static class StringExtensions
    {
        public static string EscapseSpecialCharacters(this string source)
        {
            if (string.IsNullOrWhiteSpace(source))
                return "";

            return source
                .Replace(" ", "_")
                .Replace(@"/", "_")
                .Replace("%", "_");
        }

        public static string EscapeXmlString(this string source)
        {
            if (string.IsNullOrWhiteSpace(source))
                return "";

            return source
                .Replace("&", "&amp;")
                .Replace("<", "&lt;")
                .Replace(">", "&gt;")
                .Replace("\"", "&quot;")
                .Replace("'", "&apos;");
        }
    }
}
