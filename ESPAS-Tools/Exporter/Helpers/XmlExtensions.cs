﻿using System.Xml;

namespace Exporter.Helpers
{
    public static class XmlExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="doc"></param>
        public static void CreateDeclaration(this XmlDocument doc)
        {
            XmlDeclaration decl = doc.CreateXmlDeclaration("1.0", "utf-8", "");
            doc.InsertBefore(decl, doc.DocumentElement);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static XmlNode CreateChildElement(this XmlDocument doc, string name)
        {
            var xNode = doc.CreateElement(name);
            doc.AppendChild(xNode);
            return xNode;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        /// <param name="doc"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static XmlNode CreateChildElement(this XmlNode node, XmlDocument doc, string name)
        {
            var xNode = doc.CreateElement(name);
            node.AppendChild(xNode);
            return xNode;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        /// <param name="doc"></param>
        /// <param name="name"></param>
        /// <param name="innerText"></param>
        /// <returns></returns>
        public static XmlNode CreateChildElement(this XmlNode node, XmlDocument doc, string name, string innerText)
        {
            var xNode = doc.CreateElement(name);
            xNode.InnerText = !string.IsNullOrWhiteSpace(innerText) ? innerText.EscapeXmlString() : "";

            node.AppendChild(xNode);
            return xNode;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        /// <param name="doc"></param>
        /// <param name="commentText"></param>
        public static void CreateComment(this XmlNode node, XmlDocument doc, string commentText)
        {
            var xComment = doc.CreateComment(commentText);
            node.AppendChild(xComment);
        }

        public static void AddAttribute(this XmlNode node, XmlDocument doc, string name, string value)
        {
            var attr = doc.CreateAttribute(name);
            attr.Value = value;
            node.Attributes.Append(attr);
        }
    }
}
