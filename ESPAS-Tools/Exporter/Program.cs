﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Serialization.Formatters;
using ApplicationUtilities.Utils;
using DataDomain.DataObjects;
using DataDomain.ServerClientExchange;
using DataDomain.Utils;
using log4net;
using ServerClientEventsExchange;
using ServerClientExchange.Interfaces;

namespace Exporter
{
    class Program
    {
        private static ClientAppState _clientAppState = ClientAppState.Null;
        private static ServerDispatcher _dispatcher = null;
        private static IDictionary<string, GenericPerson> _clients;
        private static IDictionary<string, PersonData> _persons;

        private static ILog _log = log4net.LogManager.GetLogger("LogFileLogger");
        private static ILog _logExporting = log4net.LogManager.GetLogger("ExportingDataLogger");

        [STAThread]
        static void Main(string[] args)
        {
            log4net.Config.XmlConfigurator.Configure();
            Console.Title = "ESPAS - Exporting";
            _log.Info("ESPAS - Exporting");

            var appSetting = new AppSetting(ConfigurationManager.AppSettings);
            try
            {
                Console.WriteLine("Connecting to server...");
                _log.Info("Connecting to server...");

                _dispatcher = CreateServerDispatcher(appSetting);
                // Call any function to test the server connection
                var securityEnabled = _dispatcher.ServerLogic.SecurityEnabled;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Cannot connect to the remote server.");
                Console.Write(ex.Message);

                _log.Error("Cannot connect to the remote server", ex);
                Console.ReadKey();
            }

            if (_dispatcher != null)
            {
                Console.WriteLine("Connect to the server successfully.");
                _log.Info("Connect to the server successfully.");

                Console.WriteLine("Logging in with Administrator account...");
                _log.Info("Logging in with Administrator account...");

                // Try to login with administrator account
                CommunicationEntity.DatabaseChangedEventMethod handler = DatabaseChangedEventMethodHandler;
                LoginReturnMsg resultMsg;
                try
                {
                    var communicationEntity = new CommunicationEntity(_dispatcher.LoginLogic);
                    resultMsg = communicationEntity.TryLogin(
                        ConfigurationManager.AppSettings["UserId"],
                        ConfigurationManager.AppSettings["GroupId"],
                        ConfigurationManager.AppSettings["Password"],
                        handler
                        );
                }
                catch (Exception ex)
                {
                    Console.WriteLine("ERROR: " + ex.Message);
                    resultMsg = new LoginReturnMsg(0);
                    _log.Error(ex.Message, ex);
                }

                if (resultMsg.SessionID > 0)
                {
                    _clientAppState = new ClientAppState(ConfigurationManager.AppSettings["UserId"], ConfigurationManager.AppSettings["GroupId"], NetHelper.GetIpAddress(), "", "", "");

                    PrintInstruction();
                    ConsoleKeyInfo pressedKey;
                    do
                    {
                        pressedKey = Console.ReadKey();
                        Console.Clear();

                        switch (pressedKey.Key)
                        {
                            case ConsoleKey.P:
                                ExportPersonInformationData();
                                Console.WriteLine("Press Ctrl+C to exit.");
                                break;
                            case ConsoleKey.J:
                                ExportPersonJournalData(_persons);
                                Console.WriteLine("Press Ctrl+C to exit.");
                                break;
                            case ConsoleKey.R:
                                ExportPersonRelationshipData(_clients);
                                Console.WriteLine("Press Ctrl+C to exit.");
                                break;
                            case ConsoleKey.A:
                                ExportPersonInformationData();
                                ExportPersonJournalData(_persons);
                                ExportPersonRelationshipData(_clients);
                                Console.WriteLine("Press Ctrl+C to exit.");
                                break;
                            default:
                                PrintInstruction();
                                break;
                        }
                    } while (!(pressedKey.Modifiers == ConsoleModifiers.Control && pressedKey.Key == ConsoleKey.C));
                }
            }
            else
            {
                Console.WriteLine("Cannot get the remote object. Please check the configuration in app.config for sure.");
                Console.WriteLine("Press Ctrl+C to exit.");
                Console.ReadKey();
            }
        }

        private static void PrintInstruction()
        {
            Console.Clear();
            Console.WriteLine("Press key for respective function:");
            Console.WriteLine("        P: export persons information to a xml file.");
            Console.WriteLine("        J: export journal data to html files.");
            Console.WriteLine("        R: export relationship data to a xml file.");
            Console.WriteLine("        A: export both of persons information and journal data.");
            Console.WriteLine("   Ctrl+C: exit");
        }

        private static void PrintHeader(string headerText)
        {
            int spacesLeft = (80 - (headerText.Length + 2)) / 2;
            int spacesRight = 80 - (headerText.Length + 2 + spacesLeft);

            Console.WriteLine(new string('=', 80));
            Console.WriteLine("=" + new string(' ', spacesLeft) + headerText + new string(' ', spacesRight) + "=");
            Console.WriteLine(new string('=', 80));

            _log.Info(new string('=', 80));
            _log.Info("=" + new string(' ', spacesLeft) + headerText + new string(' ', spacesRight) + "=");
            _log.Info(new string('=', 80));

            _logExporting.Info(new string('=', 80));
            _logExporting.Info("=" + new string(' ', spacesLeft) + headerText + new string(' ', spacesRight) + "=");
            _logExporting.Info(new string('=', 80));
        }

        private static bool LogTimeSpentEnabled()
        {
            bool logTimeSpent = false;
            var logTimeSpentStr = ConfigurationManager.AppSettings["LogTimeSpent"];
            if (!string.IsNullOrWhiteSpace(logTimeSpentStr))
            {
                logTimeSpent = logTimeSpentStr.Equals("true", StringComparison.CurrentCultureIgnoreCase) ||
                               logTimeSpentStr.Equals("yes", StringComparison.CurrentCultureIgnoreCase);
            }

            return logTimeSpent;
        }

        private static Stopwatch _watch;
        private static void ExportPersonInformationData()
        {
            PrintHeader("Exports Person Information");
            if (LogTimeSpentEnabled())
            {
                _watch = new Stopwatch();
                _watch.Start();
            }

            // Export persons data
            int numberOfConcurrentThread = 1;
            int.TryParse(ConfigurationManager.AppSettings["PI_NumberOfConcurrentThread"], out numberOfConcurrentThread);
            _log.Debug(string.Format("Number of concurrent thread:{0}", numberOfConcurrentThread));

            var personDataExport = new PersonDataExport(_dispatcher, _clientAppState, numberOfConcurrentThread)
            {
                OutputLocation = ConfigurationManager.AppSettings["PIOutputLocation"]
            };
            _log.Debug("Output location for Person Information export: " + personDataExport.OutputLocation);

            int lineNbr = Console.CursorTop + 1;
            personDataExport.Notify += (s, e) =>
            {
                Console.WriteLine(e.Message);
                _log.Info(e.Message);

                lineNbr = Console.CursorTop + 1;
            };

            personDataExport.PersonExported += (s, e) =>
            {
                _logExporting.Debug(string.Format("{0} (Id={1})", e.PersonName, e.PersonId));
                UpdateProgress(0, lineNbr, string.Format("Person {0:D4} of {1:D4}: ", e.Indexer, e.Total), e.Indexer, e.Total);
            };

            try
            {
                personDataExport.DoExport();
                _persons = personDataExport.Persons;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error happen during exporting person information data.");
                Console.WriteLine(ex.Message);

                _log.Error(ex.Message, ex);
                return;
            }

            Console.WriteLine("");
            Console.WriteLine("Exports person information >> DONE");
            _log.Info("Exports person information >> DONE");

            if (LogTimeSpentEnabled() && _watch != null)
            {
                _watch.Stop();
                Console.WriteLine(string.Format("Time spent: {0} seconds", _watch.ElapsedMilliseconds / 1000));
                _log.Debug(string.Format("Time spent: {0} seconds", _watch.ElapsedMilliseconds / 1000));

            }
            Console.WriteLine("Person Information datas are exported to: " + personDataExport.OutputLocation);
            _log.Info("Person Information datas are exported to: " + personDataExport.OutputLocation);
            Console.WriteLine("");
        }

        private static void ExportPersonJournalData(IDictionary<string, PersonData> persons)
        {
            PrintHeader("Exports Person Journal");
            if (LogTimeSpentEnabled())
            {
                _watch = new Stopwatch();
                _watch.Start();
            }

            int numberOfConcurrentThread = 1;
            int.TryParse(ConfigurationManager.AppSettings["Journal_NumberOfConcurrentThread"], out numberOfConcurrentThread);
            _log.Debug(string.Format("Number of concurrent thread:{0}", numberOfConcurrentThread));

            var journalDataExport = new JournalDataExport(_dispatcher, _clientAppState, numberOfConcurrentThread)
            {
                OutputLocation = ConfigurationManager.AppSettings["JournalOutputLocation"]
            };
            _log.Debug("Output location for Journal export: " + journalDataExport.OutputLocation);

            int lineNbr = Console.CursorTop + 1;
            journalDataExport.Notify += (s, e) =>
            {
                Console.WriteLine(e.Message);
                _log.Info(e.Message);

                lineNbr = Console.CursorTop + 1;
            };

            journalDataExport.PersonExported += (s, e) =>
            {
                _logExporting.Debug(string.Format("{0} (Id={1})", e.PersonName, e.PersonId));
                UpdateProgress(0, lineNbr, string.Format("Person {0:D4} of {1:D4}: ", e.Indexer, e.Total), e.Indexer, e.Total);
            };

            try
            {
                journalDataExport.Persons = persons;
                journalDataExport.DoExport();
                _clients = journalDataExport.Clients;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error happen during exporting person journal data");
                Console.WriteLine(ex.Message);

                _log.Error(ex.Message, ex);
                return;
            }

            Console.WriteLine("");
            Console.WriteLine("Exports journal data >> DONE");
            _log.Info("Exports journal data >> DONE");

            if (LogTimeSpentEnabled() && _watch != null)
            {
                _watch.Stop();
                Console.WriteLine(string.Format("Time spent: {0} seconds", _watch.ElapsedMilliseconds / 1000));
                _log.Debug(string.Format("Time spent: {0} seconds", _watch.ElapsedMilliseconds / 1000));
            }
            Console.WriteLine("Person Journal datas are exported to: " + journalDataExport.OutputLocation);
            _logExporting.Info("Person Journal datas are exported to: " + journalDataExport.OutputLocation);

            Console.WriteLine("");
        }

        private static void ExportPersonRelationshipData(IDictionary<string, GenericPerson> clients)
        {
            PrintHeader("Exports Person Relationships");
            if (LogTimeSpentEnabled())
            {
                _watch = new Stopwatch();
                _watch.Start();
            }

            var relationshipDataExport = new RelationshipDataExport(_dispatcher, _clientAppState)
            {
                OutputLocation = ConfigurationManager.AppSettings["RelationshipOutputLocation"]
            };
            _log.Debug("Output location for Relationships export: " + relationshipDataExport.OutputLocation);

            int lineNbr = Console.CursorTop + 1;
            relationshipDataExport.Notify += (s, e) =>
            {
                Console.WriteLine(e.Message);
                _log.Info(e.Message);

                lineNbr = Console.CursorTop + 1;
            };

            relationshipDataExport.PersonExported += (s, e) =>
            {
                _logExporting.Debug(string.Format("{0} (Id={1})", e.PersonName, e.PersonId));
                UpdateProgress(0, lineNbr, string.Format("Person {0:D4} of {1:D4}: ", e.Indexer, e.Total), e.Indexer, e.Total);
            };

            try
            {
                relationshipDataExport.Clients = clients;
                relationshipDataExport.DoExport();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error happen during exporting person relationship data");
                Console.WriteLine(ex.Message);

                _log.Error(ex.Message, ex);
                return;
            }

            Console.WriteLine("");
            Console.WriteLine("Exports relationship data >> DONE");
            _log.Info("Exports relationship data >> DONE");

            if (LogTimeSpentEnabled() && _watch != null)
            {
                _watch.Stop();
                Console.WriteLine(string.Format("Time spent: {0} seconds", _watch.ElapsedMilliseconds / 1000));
                _log.Debug(string.Format("Time spent: {0} seconds", _watch.ElapsedMilliseconds / 1000));
            }
            Console.WriteLine("Person Relationship datas are exported to: " + relationshipDataExport.OutputLocation);
            _logExporting.Info("Person Relationship datas are exported to: " + relationshipDataExport.OutputLocation);

            Console.WriteLine("");
        }

        private static void DatabaseChangedEventMethodHandler(ChangeEventData changeEventData)
        {
            //TODO:
            // Do nothing...
        }

        private static ServerDispatcher CreateServerDispatcher(AppSetting appSetting)
        {
            var serverProvider = new BinaryServerFormatterSinkProvider
            {
                TypeFilterLevel = TypeFilterLevel.Full
            };
            var clientProvider = new BinaryClientFormatterSinkProvider();

            IDictionary props = new Hashtable();
            props["port"] = 0;
            props["name"] = string.Format("{0}:{1}", appSetting.ServerUrl, appSetting.ServerPort);

            IChannel channel = new TcpChannel(props, clientProvider, serverProvider);
            ChannelServices.RegisterChannel(channel, false);

            string url = string.Format("tcp://{0}:{1}/{2}",
                appSetting.ServerUrl,
                appSetting.ServerPort,
                appSetting.RemotePublishedObject);

            var proxyGenericServer = Activator.GetObject(typeof(IGenericServer), url) as IGenericServer;
            return new ServerDispatcher(proxyGenericServer);
        }

        static private readonly object _sync = new object();
        private static void UpdateProgress(int x, int y, string item, int progress, int total)
        {
            int percentage = (int)100.0 * progress / total;
            lock (_sync)
            {
                Console.CursorLeft = x;
                Console.CursorTop = y;
                Console.Write(item + " [" + new string('=', percentage / 2) + "] " + percentage + "%");
            }
        }
    }
}
