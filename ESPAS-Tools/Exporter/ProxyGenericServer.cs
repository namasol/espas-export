﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ApplicationUtilities.ErrorReporting;
using ApplicationUtilities.ErrorReporting.Upload;
using ApplicationUtilities.Utils;
using DataDomain.ApplicationDefinition;
using DataDomain.ApplicationDefinition.OfficeTemplates;
using DataDomain.DataObjects;
using DataDomain.DataObjects.Medication;
using DataDomain.DataObjects.Office;
using DataDomain.DataObjects.Scheduler;
using DataDomain.DataObjects.SpellChecker;
using DataDomain.DataSets;
using DataDomain.DataStructures.Tree.Generic;
using DataDomain.Filters;
using DataDomain.GridLayout;
using DataDomain.Utils;
using log4net;
using ServerClientEventsExchange;
using ServerClientExchange.Interfaces;

namespace Exporter
{
    public class ProxyGenericServer : IGenericServer
    {
        private readonly static ILog Log = LogManager.GetLogger(typeof(ProxyGenericServer));

        private readonly IGenericServer _genericServer;

        public ProxyGenericServer(IGenericServer genericServer)
        {
            _genericServer = genericServer;
        }

        private object ExecuteFunctionWithResult(Func<object> fnx)
        {
            try
            {
                return fnx.Invoke();
            }
            catch (Exception ex)
            {
                string errorText = ex.Message;
                if (ex.InnerException != null)
                {
                    errorText += "\n" + ex.InnerException.Message;
                }

                Log.Error(errorText);
            }
            return null;
        }

        public DateTime GetCurrentDate()
        {
            return (DateTime)ExecuteFunctionWithResult(() => _genericServer.GetCurrentDate());
        }

        #region "Person Information"

        ///<inheritdoc />
        public PersonData[] GetPersonsWithDataItem(ClientAppState clientAppState)
        {
            return ExecuteFunctionWithResult(() => _genericServer.GetPersonsWithDataItem(clientAppState)) as PersonData[];
        }

        ///<inheritdoc />
        public PersonDataCollection GetPersons(ClientAppState clientAppState)
        {
            return ExecuteFunctionWithResult(() => _genericServer.GetPersons(clientAppState)) as PersonDataCollection;
        }

        ///<inheritdoc />
        public AbstractGenericData GetFieldData(string personId, string fieldName, DateTime date)
        {
            return ExecuteFunctionWithResult(() => _genericServer.GetFieldData(personId, fieldName, date)) as AbstractGenericData;
        }

        ///<inheritdoc />
        public Hashtable GetFieldsData(string personId, AbstractMappedItem[] fields, DateTime date)
        {
            return ExecuteFunctionWithResult(() => _genericServer.GetFieldsData(personId, fields, date)) as Hashtable;
        }

        #endregion

        #region "Advanced Search"

        public VersionedPersonSearchResult GetPersonSearchResult(ClientAppState state, string version, bool includeInactivePerson)
        {
            return ExecuteFunctionWithResult(() => _genericServer.GetPersonSearchResult(state, version, includeInactivePerson)) as VersionedPersonSearchResult;
        }

        #endregion

        #region "Generic Logic"

        ///<inheritdoc />
        public ApplicationDefinitionData GetApplicationDefinitionData()
        {
            return ExecuteFunctionWithResult(() => _genericServer.GetApplicationDefinitionData()) as ApplicationDefinitionData;
        }

        #endregion

        #region "Relationship Diagrams"

        public DiagramData GetPersonDiagramData(ClientAppState clientAppState, string personId, DateTime date)
        {
            return ExecuteFunctionWithResult(() => _genericServer.GetPersonDiagramData(clientAppState, personId, date)) as DiagramData;
        }

        #endregion

        ///<inheritdoc />
        public AbstractGenericData GetGridFieldDefaultRowData(ClientAppState clientAppState, string personId, string fieldName,
            DateTime date)
        {
            throw new NotImplementedException();
        }

        ///<inheritdoc />
        public AbstractGenericData GetMemberData(string personId, string fieldName, DateTime date)
        {
            throw new NotImplementedException();
        }

        public GenericPerson GetGenericPerson(string personId)
        {
            throw new NotImplementedException();
        }

        public PersonData[] GetRelatedPersonsByRole(string personId, DateTime date, string role)
        {
            throw new NotImplementedException();
        }

        public IClientDiagramHelper GetClientDiagramHelper()
        {
            throw new NotImplementedException();
        }

        public ApplicationDefinitionData ApplicationDefinitionData { get; private set; }

        public PersonData GetPersonData(string personId)
        {
            throw new NotImplementedException();
        }

        public PersonData GetPersonData(ClientAppState clientAppState, string personId)
        {
            throw new NotImplementedException();
        }

        public PersonDataCollection GetPersons(ClientAppState clientAppState, string groupId, string categoryId,
            bool getPersonsMarkedAsDeleted)
        {
            throw new NotImplementedException();
        }

        public PersonDataCollection GetPersons(ClientAppState clientAppState, string groupId, Category.CategoryRoles categoryRole,
            bool getPersonsMarkedAsDeleted)
        {
            throw new NotImplementedException();
        }

        public PersonDataCollection GetPersons(ClientAppState clientAppState, string groupId, bool getPersonsMarkedAsDeleted)
        {
            throw new NotImplementedException();
        }

        public Dictionary<string, List<PersonData>> GetPersonsGroupedByGroup(ClientAppState clientAppState, IEnumerable<string> groupsId, bool getPersonsMarkedAsDeleted,
            bool excludeNotGroupSpecificPersons)
        {
            throw new NotImplementedException();
        }

        public Dictionary<string, List<PersonData>> GetPersonsGroupedByGroup(ClientAppState clientAppState, IEnumerable<string> groupsId, bool getPersonsMarkedAsDeleted,
            bool excludeNotGroupSpecificPersons, DateTimeUtils.TimeInterval interval)
        {
            throw new NotImplementedException();
        }

        public Dictionary<string, List<ClientData>> GetClientsWithSelectableDataGroupedByGroup(ClientAppState clientAppState, IEnumerable<string> groupsId,
            bool excludeNotGroupSpecificPersons, bool getInactivePersons)
        {
            throw new NotImplementedException();
        }

        public Dictionary<string, List<ClientData>> GetClientsWithSelectableDataGroupedByGroup(ClientAppState clientAppState, IEnumerable<string> groupsId,
            bool excludeNonGroupSpecificCategories, bool getInactivePersons, DateTimeUtils.TimeInterval interval)
        {
            throw new NotImplementedException();
        }

        public PersonDataCollection GetPersons(ClientAppState clientAppState, string groupId, string categoryId)
        {
            throw new NotImplementedException();
        }

        public PersonDataCollection GetPersons(ClientAppState clientAppState, string groupId, Category.CategoryRoles categoryRole)
        {
            throw new NotImplementedException();
        }

        public PersonDataCollection GetPersons(ClientAppState clientAppState, string groupId)
        {
            throw new NotImplementedException();
        }

        public int GetNumberOfPersons(ClientAppState clientAppState, string groupId, string categoryId, bool getPersonsMarkedAsDeleted)
        {
            throw new NotImplementedException();
        }

        public int GetNumberOfPersons(ClientAppState clientAppState, string groupId, Category.CategoryRoles categoryRole,
            bool getPersonsMarkedAsDeleted)
        {
            throw new NotImplementedException();
        }

        public int GetNumberOfPersons(ClientAppState clientAppState, string groupId, bool getPersonsMarkedAsDeleted)
        {
            throw new NotImplementedException();
        }

        public int GetNumberOfPersons(ClientAppState clientAppState, string groupId, string categoryId)
        {
            throw new NotImplementedException();
        }

        public int GetNumberOfPersons(ClientAppState clientAppState, string groupId, Category.CategoryRoles categoryRole)
        {
            throw new NotImplementedException();
        }

        public int GetNumberOfPersons(ClientAppState clientAppState, string groupId)
        {
            throw new NotImplementedException();
        }

        public PersonData GetLastClientAdded(ClientAppState clientAppState)
        {
            throw new NotImplementedException();
        }

        public void ImportUsers(ClientAppState clientAppState, byte[] stream)
        {
            throw new NotImplementedException();
        }

        public void ImportUsers(ClientAppState clientAppState, string usersFilePath)
        {
            throw new NotImplementedException();
        }

        public void UpdatePersonData(ClientAppState clientAppState, PersonData personData)
        {
            throw new NotImplementedException();
        }

        public void UpdatePersonData(ClientAppState clientAppState, PersonData personData, string[] groupsToActivate)
        {
            throw new NotImplementedException();
        }

        public void UpdatePersonDataFromImporter(ClientAppState clientAppState, PersonData personData, string groupId)
        {
            throw new NotImplementedException();
        }

        public void AddPerson(ClientAppState clientAppState, PersonData personData)
        {
            throw new NotImplementedException();
        }

        public void AddPersonFromImporter(ClientAppState clientAppState, PersonData personData, string groupId)
        {
            throw new NotImplementedException();
        }

        public void SetPersonCategory(ClientAppState clientAppState, PersonData person, string categoryId)
        {
            throw new NotImplementedException();
        }

        public void AddPersonImage(ClientAppState clientAppState, string personId, byte[] stream)
        {
            throw new NotImplementedException();
        }

        public void RemovePerson(ClientAppState clientAppState, PersonData personData)
        {
            throw new NotImplementedException();
        }

        public PersonStateEnum AllowRemovePerson(ClientAppState clientAppState, string personId)
        {
            throw new NotImplementedException();
        }

        public byte[] GetPersonImage(ClientAppState clientAppState, string personId)
        {
            throw new NotImplementedException();
        }

        public void SetFieldData(ClientAppState clientAppState, string personId, string fieldName, AbstractGenericData value)
        {
            throw new NotImplementedException();
        }

        public void SetFieldData(ClientAppState clientAppState, string personId, string fieldName, AbstractGenericData value,
            bool updateResource)
        {
            throw new NotImplementedException();
        }

        public void SetDataMember(ClientAppState clientAppState, string personId, string fieldName, AbstractGenericData value)
        {
            throw new NotImplementedException();
        }

        public SortedList GetPersonFieldEntries(ClientAppState clientAppState, string personId, string fieldName)
        {
            throw new NotImplementedException();
        }

        public DateTime GetPreviousDateFields(ClientAppState clientAppState, string personId, DateTime currentDate)
        {
            throw new NotImplementedException();
        }

        public DateTime GetNextDateFields(ClientAppState clientAppState, string personId, DateTime currentDate)
        {
            throw new NotImplementedException();
        }

        public DateTime GetPreviousDateDiagrams(ClientAppState clientAppState, string personId, DateTime currentDate)
        {
            throw new NotImplementedException();
        }

        public DateTime GetNextDateDiagrams(ClientAppState clientAppState, string personId, DateTime currentDate)
        {
            throw new NotImplementedException();
        }

        public OwnerDiagramData GetPersonDiagramDataWithOwner(ClientAppState clientAppState, string personId, DateTime date)
        {
            throw new NotImplementedException();
        }

        public byte[] GetPersonDiagramDataImage(ClientAppState clientAppState, string personId, DateTime date, int width, int height,
            string culture)
        {
            throw new NotImplementedException();
        }

        public byte[] GetPersonDiagramDataImage(ClientAppState clientAppState, string personId, DateTime date, string culture)
        {
            throw new NotImplementedException();
        }

        public byte[] GetPersonDiagramDataImage(ClientAppState clientAppState, string personId, DateTime date, int zoom, string culture)
        {
            throw new NotImplementedException();
        }

        public void AddDiagramPersonData(ClientAppState clientAppState, string personId, DiagramEntityData diagramPersonData)
        {
            throw new NotImplementedException();
        }

        public void RemoveDiagramPersonData(ClientAppState clientAppState, string personId, DiagramEntityData diagramPersonData)
        {
            throw new NotImplementedException();
        }

        public void AddDiagramRelationData(ClientAppState clientAppState, string personId, DiagramRelationData diagramRelationData)
        {
            throw new NotImplementedException();
        }

        public void AddDiagramRelationData(ClientAppState clientAppState, string personId, DiagramRelationData diagramRelationData,
            bool doNotUpdateIfPresent)
        {
            throw new NotImplementedException();
        }

        public void RemoveDiagramRelationData(ClientAppState clientAppState, string personId, DiagramRelationData diagramRelationData)
        {
            throw new NotImplementedException();
        }

        public PersonDataCollection GetClientsRelatedToPerson(ClientAppState clientAppState, string personId, string groupId)
        {
            throw new NotImplementedException();
        }

        public PersonDataCollection GetSelectablePersonsInCurrentDiagram(ClientAppState clientAppState, string clientId)
        {
            throw new NotImplementedException();
        }

        public PersonDataCollection GetNonSelectablePersons(ClientAppState clientAppState, string clientId, string toolName,
            string activityId)
        {
            throw new NotImplementedException();
        }

        public DiagramData GetPersonSphereDiagramData(ClientAppState clientAppState, string personId, string activityId)
        {
            throw new NotImplementedException();
        }

        public byte[] GetPersonSphereDiagramDataImage(ClientAppState clientAppState, string personId, string activityId, int zoom,
            string culture)
        {
            throw new NotImplementedException();
        }

        public System.Drawing.Size GetPersonSphereDiagramDataImageSize(ClientAppState clientAppState, string personId, string activityId, int zoom,
            string culture)
        {
            throw new NotImplementedException();
        }

        public void AddSphereDiagramEntityData(ClientAppState clientAppState, string personId, string activityId,
            DiagramEntityData diagramEntityData, DiagramRelationDataCollection diagramRelations)
        {
            throw new NotImplementedException();
        }

        public void UpdateSphereDiagramEntityData(ClientAppState clientAppState, string personId, string activityId,
            DiagramEntityData diagramEntityData)
        {
            throw new NotImplementedException();
        }

        public void RemoveSphereDiagramEntityData(ClientAppState clientAppState, string personId, string activityId,
            DiagramEntityData diagramEntityData)
        {
            throw new NotImplementedException();
        }

        public void AddSpehreDiagramRelationData(ClientAppState clientAppState, string personId, string activityId,
            DiagramRelationData diagramRelationData)
        {
            throw new NotImplementedException();
        }

        public void RemoveSphereDiagramRelationData(ClientAppState clientAppState, string personId, string activityId,
            DiagramRelationData diagramRelationData)
        {
            throw new NotImplementedException();
        }

        public void SaveSphereDiagramTemplate(ClientAppState clientAppState, string name, DiagramData diagramData)
        {
            throw new NotImplementedException();
        }

        public void CreateSphereDiagramFromTemplate(ClientAppState clientAppState, string personId, string activityId, ArrayList names)
        {
            throw new NotImplementedException();
        }

        public ArrayList GetPersonDiagramsDate(ClientAppState clientAppState, string personId, DateTime date)
        {
            throw new NotImplementedException();
        }

        public SortedList GetPersonDiagrams(ClientAppState clientAppState, string personId)
        {
            throw new NotImplementedException();
        }

        public void AddPersonDescriptionDataItem(ClientAppState clientAppState, string personId, string activityId,
            DescriptionDataItem descriptionDataItem)
        {
            throw new NotImplementedException();
        }

        public void RemovePersonDescriptionDataItem(ClientAppState clientAppState, string personId, string activityId,
            DescriptionDataItem descriptionDataItem)
        {
            throw new NotImplementedException();
        }

        public void SetProcessManagementFinished(ClientAppState clientAppState, string personId, string activityId, bool finished)
        {
            throw new NotImplementedException();
        }

        public void ResetActivity(ClientAppState clientAppState, string personId, string processId, int phaseIdx, int activityIdx)
        {
            throw new NotImplementedException();
        }

        public int AddJournalEntryData(ClientAppState clientAppState, JournalEntryData journalEntryData, string clientId,
            bool showEntriesForFirstAbout, IFilterExpression<JournalEntryData> filter)
        {
            throw new NotImplementedException();
        }

        public void RemovePersonJournalEntryData(ClientAppState clientAppState, string personId, string id)
        {
            throw new NotImplementedException();
        }

        public ArrayList GetPersonJournalData(ClientAppState clientAppState, string personId, bool showEntriesForFirstAbout)
        {
            throw new NotImplementedException();
        }

        public int GetPersonJournalEntryPosition(ClientAppState clientAppState, string personId, string rowId,
            bool showEntriesForFirstAbout)
        {
            throw new NotImplementedException();
        }

        public ArrayList GetPersonJournalData(ClientAppState clientAppState, string personId, int startIndex, int rowsNo,
            IFilterExpression<JournalEntryData> filter, bool showEntriesForFirstAbout)
        {
            throw new NotImplementedException();
        }

        public ArrayList GetPersonJournalData(ClientAppState clientAppState, string personId, IFilterExpression<JournalEntryData> filter,
            bool showEntriesForFirstAbout)
        {
            throw new NotImplementedException();
        }

        public int GetPersonJournalDataRowsNo(ClientAppState clientAppState, string personId, IFilterExpression<JournalEntryData> filter,
            bool showEntriesForFirstAbout)
        {
            throw new NotImplementedException();
        }

        public ArrayList GetGroupJournalData(ClientAppState clientAppState, string groupId, bool excludeNonGroupSpecificPersons)
        {
            throw new NotImplementedException();
        }

        public int GetGroupJournalDataRowsNo(ClientAppState clientAppState, string groupId)
        {
            throw new NotImplementedException();
        }

        public ArrayList GetGroupJournalData(ClientAppState clientAppState, string groupId, bool excludeNonGroupSpecificPersons,
            int startIndex, int rowsNo)
        {
            throw new NotImplementedException();
        }

        public ArrayList GetNonGroupSpecificClientsJournalData(ClientAppState clientAppState)
        {
            throw new NotImplementedException();
        }

        public JournalEntryData GetJournalEntryData(ClientAppState clientAppState, string groupId, string id)
        {
            throw new NotImplementedException();
        }

        public Hashtable GetJournalEntriesAuthorsProffesion(ClientAppState clientAppState, string personId)
        {
            throw new NotImplementedException();
        }

        public void AddJournalAttachment(ClientAppState clientAppState, string groupId, string journalEntryId, byte[] stream,
            string fullName, int position)
        {
            throw new NotImplementedException();
        }

        public void AddJournalAttachment(ClientAppState clientAppState, string groupId, string journalEntryId, byte[] stream,
            string fullName)
        {
            throw new NotImplementedException();
        }

        public void UpdateJournalAttachment(ClientAppState clientAppState, string clientId, string journalEntryId, byte[] content,
            int position)
        {
            throw new NotImplementedException();
        }

        public void RemoveJournalAttachment(ClientAppState clientAppState, string groupId, string journalEntryId, int position)
        {
            throw new NotImplementedException();
        }

        public object[] GetJournalEntryAttachmentFile(ClientAppState clientAppState, string groupId, string journalEntryId,
            int position)
        {
            throw new NotImplementedException();
        }

        public void OpenJournalEntryAttachment(ClientAppState clientAppState, string groupId, string journalEntryId, int position)
        {
            throw new NotImplementedException();
        }

        public ProcessDataCollection GetPersonProcesses(ClientAppState clientAppState, string personId)
        {
            throw new NotImplementedException();
        }

        public void AddPersonProcess(ClientAppState clientAppState, string personId, ProcessData process, DateTime startDate)
        {
            throw new NotImplementedException();
        }

        public void RemovePersonProcess(ClientAppState clientAppState, string personId, int processIdx)
        {
            throw new NotImplementedException();
        }

        public void ClosePersonProcess(ClientAppState clientAppState, string personId, int processIdx, DateTime closeDate)
        {
            throw new NotImplementedException();
        }

        public void UpdateProcessManagementChange(ClientAppState clientAppState, string personId, ProcessManagementChange change)
        {
            throw new NotImplementedException();
        }

        public ActivityData GetActivityByID(ClientAppState clientAppState, string personId, string activityId)
        {
            throw new NotImplementedException();
        }

        public PhaseData GetPhaseByActivityID(ClientAppState clientAppState, string personId, string activityId)
        {
            throw new NotImplementedException();
        }

        public PhaseData GetPhaseByDate(ClientAppState clientAppState, string personId, DateTime date)
        {
            throw new NotImplementedException();
        }

        public string GetActivityIDByDate(ClientAppState clientAppState, string personId, DateTime date, string toolName)
        {
            throw new NotImplementedException();
        }

        public Hashtable GetPersonGoalsData(ClientAppState clientAppState, string personId, string activityId)
        {
            throw new NotImplementedException();
        }

        public void CreatePersonGoalsData(ClientAppState clientAppState, string personId, string activityId)
        {
            throw new NotImplementedException();
        }

        public GoalDefinitionItemData GetGoalDefinitionItemData(ClientAppState clientAppState, string personId, string activityId,
            string goalId)
        {
            throw new NotImplementedException();
        }

        public void AddGoalDefinitionItem(ClientAppState clientAppState, string personId, string activityId,
            GoalDefinitionItemData data)
        {
            throw new NotImplementedException();
        }

        public void RemoveGoalDefinitionItem(ClientAppState clientAppState, string personId, string activityId, string goalId)
        {
            throw new NotImplementedException();
        }

        public void UpdateGoalsDefinition(ClientAppState clientAppState, string personId, string activityId, ArrayList goals)
        {
            throw new NotImplementedException();
        }

        public Hashtable GetAllGoalEffects(ClientAppState clientAppState, string personId, string activityId)
        {
            throw new NotImplementedException();
        }

        public ArrayList GetGoalEffects(ClientAppState clientAppState, string personId, string activityId, string goalId)
        {
            throw new NotImplementedException();
        }

        public void SetGoalEffects(ClientAppState clientAppState, string personId, string activityId, string goalId, ArrayList effects)
        {
            throw new NotImplementedException();
        }

        public ArrayList GetGoalActionSteps(ClientAppState clientAppState, string personId, string activityId, string goalId)
        {
            throw new NotImplementedException();
        }

        public string GetProductGroupForGoal(ClientAppState clientAppState, string goalWeekReference, string personId,
            string activityId)
        {
            throw new NotImplementedException();
        }

        public void SetGoalActionSteps(ClientAppState clientAppState, string personId, string activityId, string goalId,
            ArrayList actions)
        {
            throw new NotImplementedException();
        }

        public ArrayList GetGoalRestrictions(ClientAppState clientAppState, string personId, string activityId, string goalId)
        {
            throw new NotImplementedException();
        }

        public void SetGoalRestrictions(ClientAppState clientAppState, string personId, string activityId, string goalId,
            ArrayList restrictions)
        {
            throw new NotImplementedException();
        }

        public ArrayList GetGoalBestWorstCases(ClientAppState clientAppState, string personId, string activityId, string goalId)
        {
            throw new NotImplementedException();
        }

        public void SetGoalBestWorstCases(ClientAppState clientAppState, string personId, string activityId, string goalId,
            ArrayList bestWorstCases)
        {
            throw new NotImplementedException();
        }

        public string GetApplicationActivityID(ClientAppState clientAppState, string personId, string activityId, string toolName)
        {
            throw new NotImplementedException();
        }

        public ArrayList GetGoalAchievedThrough(ClientAppState clientAppState, string personId, string activityId, string goalId)
        {
            throw new NotImplementedException();
        }

        public void SetGoalAchievedThrough(ClientAppState clientAppState, string personId, string activityId, string goalId,
            ArrayList achievedThroughData)
        {
            throw new NotImplementedException();
        }

        public ArrayList GetGoalSelectedActionSteps(ClientAppState clientAppState, string personId, string activityId, string goalId)
        {
            throw new NotImplementedException();
        }

        public void SetGoalSelectedActionSteps(ClientAppState clientAppState, string personId, string activityId, string goalId,
            ArrayList selectedActionSteps)
        {
            throw new NotImplementedException();
        }

        public ArrayList GetGoalEvaluations(ClientAppState clientAppState, string personId, string activityId, string goalId)
        {
            throw new NotImplementedException();
        }

        public void SetGoalEvaluations(ClientAppState clientAppState, string personId, string activityId, string goalId,
            ArrayList goalEvaluationsData)
        {
            throw new NotImplementedException();
        }

        public ArrayList GetMentalModels(ClientAppState clientAppState, string personId, string activityId)
        {
            throw new NotImplementedException();
        }

        public void SetMentalModel(ClientAppState clientAppState, string personId, string activityId, int modelIdx, ArrayList data,
            bool update)
        {
            throw new NotImplementedException();
        }

        public void RemoveMentalModel(ClientAppState clientAppState, string personId, string activityId, int modelIdx)
        {
            throw new NotImplementedException();
        }

        public void CreateMentalModels(ClientAppState clientAppState, string personId, string activityId)
        {
            throw new NotImplementedException();
        }

        public StringArray GetGoalActionPlanChildren(ClientAppState clientAppState, string personId, string activityId, string goalId)
        {
            throw new NotImplementedException();
        }

        public void UpdateGoalActionPlanChildren(ClientAppState clientAppState, string personId, string activityId, string goalId,
            StringArray children)
        {
            throw new NotImplementedException();
        }

        public StringArray ImportGoalActionPlanChildrenCommand(ClientAppState clientAppState, string personId, string activityId,
            string goalId)
        {
            throw new NotImplementedException();
        }

        public void AddPersonDiagnose(ClientAppState clientAppState, string personId, string activityId, string diagnoseText)
        {
            throw new NotImplementedException();
        }

        public string GetPersonDiagnose(ClientAppState clientAppState, string personId, string activityId)
        {
            throw new NotImplementedException();
        }

        public void CreatePersonDiagnosis(ClientAppState clientAppState, string personId, string activityId)
        {
            throw new NotImplementedException();
        }

        public bool IsActivityReadonly(ClientAppState clientAppState, string personId, string activityId)
        {
            throw new NotImplementedException();
        }

        public ActivityState GetActivityState(ClientAppState clientAppState, string personId, string activityId)
        {
            throw new NotImplementedException();
        }

        public DescriptionDataTitleItemCollection GetDescriptionDataTitleItems(ClientAppState clientAppState, string personId,
            string activityId, string titleId)
        {
            throw new NotImplementedException();
        }

        public void AddDescriptionDataTitle(ClientAppState clientAppState, string personId, string activityId, string titleId,
            DescriptionDataTitleItem data)
        {
            throw new NotImplementedException();
        }

        public void RemoveDescriptionDataTitle(ClientAppState clientAppState, string personId, string activityId, string titleId,
            string dataItemId)
        {
            throw new NotImplementedException();
        }

        public void CreatePersonDescriptionTitleData(ClientAppState clientAppState, string personId, string activityId)
        {
            throw new NotImplementedException();
        }

        public DescriptionTitleData GetPersonDescriptionTitleData(ClientAppState clientAppState, string personId, string activityId)
        {
            throw new NotImplementedException();
        }

        public Hashtable GetTimeSpent(ClientAppState clientAppState, string personId, string activityId, StringArray journalEntryTypes)
        {
            throw new NotImplementedException();
        }

        public void DebugDatabaseConnection(ClientAppState clientAppState, bool open)
        {
            throw new NotImplementedException();
        }

        public void AddPersonEvaluation(ClientAppState clientAppState, string personId, string activityId, string evaluationText)
        {
            throw new NotImplementedException();
        }

        public string GetPersonEvaluation(ClientAppState clientAppState, string personId, string activityId)
        {
            throw new NotImplementedException();
        }

        public ArrayList GetGoalEffectEvaluations(ClientAppState clientAppState, string personId, string activityId, string goalId)
        {
            throw new NotImplementedException();
        }

        public void SetGoalEffectEvaluations(ClientAppState clientAppState, string personId, string activityId, string goalId,
            ArrayList goalEffectEvaluations)
        {
            throw new NotImplementedException();
        }

        public void ExecuteAggregateCommand(ClientAppState clientAppState, int noCommands)
        {
            throw new NotImplementedException();
        }

        public EffectEvaluatorArray GetPersonEvaluators(ClientAppState clientAppState, string personId, string activityId)
        {
            throw new NotImplementedException();
        }

        public void SynchronizeEvaluators(ClientAppState clientAppState, string personId, string activityId,
            EffectEvaluatorArray evaluators, int deletedIndex)
        {
            throw new NotImplementedException();
        }

        public DiagnoseEvaluationData GetDiagnoseEvaluationData(ClientAppState clientAppState, string personId, string activityId)
        {
            throw new NotImplementedException();
        }

        public void AddDiagnoseEvaluationData(ClientAppState clientAppState, string personId, string activityId,
            DiagnoseEvaluationItemData data)
        {
            throw new NotImplementedException();
        }

        public void RemoveDiagnoseEvaluationData(ClientAppState clientAppState, string personId, string activityId, string itemId)
        {
            throw new NotImplementedException();
        }

        public void CreateDiagnoseEvaluationData(ClientAppState clientAppState, string clientId, string activityId)
        {
            throw new NotImplementedException();
        }

        public void DebugSetStartDataForActivity(ClientAppState clientAppState, string personId, string activityId, string startData)
        {
            throw new NotImplementedException();
        }

        public void UpdatePersonGoalsData(ClientAppState clientAppState)
        {
            throw new NotImplementedException();
        }

        public void UpdatePersonRtfToPlainText(ClientAppState clientAppState)
        {
            throw new NotImplementedException();
        }

        public ArrayList GetPersonSpheres(ClientAppState clientAppState, string personId)
        {
            throw new NotImplementedException();
        }

        public void AddSphere(ClientAppState clientAppState, SphereData sphere)
        {
            throw new NotImplementedException();
        }

        public void DebugRemoveSpheres(ClientAppState clientAppState)
        {
            throw new NotImplementedException();
        }

        public void DebugRemoveSphere(ClientAppState clientAppState, SphereData sphere)
        {
            throw new NotImplementedException();
        }

        public void RemoveSphereData(ClientAppState clientAppState, string sphereId)
        {
            throw new NotImplementedException();
        }

        public bool IsSphereUsedInOtherDiagrams(ClientAppState clientAppState, string sphereId)
        {
            throw new NotImplementedException();
        }

        public void MarkSphereAsDeleted(ClientAppState clientAppState, string sphereId)
        {
            throw new NotImplementedException();
        }

        public bool DetermineDiagramTemplateExistence(ClientAppState clientAppState, string templateType)
        {
            throw new NotImplementedException();
        }

        public ArrayList GetSelectedSpheresAndRelationInfo(ClientAppState clientAppState, string personId, string activityId)
        {
            throw new NotImplementedException();
        }

        public ArrayList GetSelectedSpheres(ClientAppState clientAppState, string personId, string activityId)
        {
            throw new NotImplementedException();
        }

        public bool IsClassificationNodeUsedByOtherClients(ClientAppState clientAppState, string personId, string nodeId)
        {
            throw new NotImplementedException();
        }

        public ArrayList GetPersonClassificationData(ClientAppState clientAppState, string activityId, string personId)
        {
            throw new NotImplementedException();
        }

        public void CreatePersonClassificationData(ClientAppState clientAppState, string activityId, string personId)
        {
            throw new NotImplementedException();
        }

        public void SaveClassificationData(ClientAppState clientAppState, string personId, string activityId, string sphereId,
            ClassificationDataBase data)
        {
            throw new NotImplementedException();
        }

        public void MarkClassificationDataNode(ClientAppState clientAppState, string activityId, string personId, string sphereId,
            string nodeId)
        {
            throw new NotImplementedException();
        }

        public void UpdateClassificationLevelTreeAndClassificationData(ClientAppState clientAppState, ITree<ClassificationLevel> newTree)
        {
            throw new NotImplementedException();
        }

        public void SetClassificationNodesMarkValue(ClientAppState clientAppState, string activityId, string personId, string sphereId,
            ArrayList nodesId)
        {
            throw new NotImplementedException();
        }

        public IDictionary<string, string> GetUserApplicationData(ClientAppState clientAppState, string userId, string personId, string toolName)
        {
            throw new NotImplementedException();
        }

        public IDictionary<string, string> GetUserApplicationData(ClientAppState clientAppState, string userId, string toolName)
        {
            throw new NotImplementedException();
        }

        public void SetUserApplicationData(ClientAppState clientAppState, string userId, string groupId, string personId,
            string toolName, IDictionary<string, string> userSelection)
        {
            throw new NotImplementedException();
        }

        public PersonToolData GetLastPersonAndToolUsed(ClientAppState clientAppState, string userId, string groupId)
        {
            throw new NotImplementedException();
        }

        public string GetLastClient(ClientAppState clientAppState, string userId)
        {
            throw new NotImplementedException();
        }

        public string GetLastToolUsed(ClientAppState clientAppState, string userId, string personId)
        {
            throw new NotImplementedException();
        }

        public void SetLastToolUsed(ClientAppState clientAppState, string userId, string personId, string toolName)
        {
            throw new NotImplementedException();
        }

        public PersonData[] GetMyClients(ClientAppState clientAppState, string userId, string currentGroupId)
        {
            throw new NotImplementedException();
        }

        public void SetMyClients(ClientAppState clientAppState, string userId, string[] myClientsIds)
        {
            throw new NotImplementedException();
        }

        public IDictionary<string, string> GetUserToolSelection(ClientAppState clientAppState)
        {
            throw new NotImplementedException();
        }

        public void SetUserToolSelection(ClientAppState clientAppState, IDictionary<string, string> keyToValue)
        {
            throw new NotImplementedException();
        }

        public ArrayList GetIntegrationAnalysisData(ClientAppState clientAppState, string personId, string activityId)
        {
            throw new NotImplementedException();
        }

        public void SetIntegrationAnalysisData(ClientAppState clientAppState, string personId, string activityId, string sphereId,
            IntegrationAnalysisData integrationAnalysisData)
        {
            throw new NotImplementedException();
        }

        public void CreateIntegrationAnalysisData(ClientAppState clientAppState, string personId, string activityId)
        {
            throw new NotImplementedException();
        }

        public ArrayList GetAllowedScopes(ClientAppState clientAppState, string personId, string groupId, SphereData sphereToEdit)
        {
            throw new NotImplementedException();
        }

        public Hashtable GetClassificationSelectedPersons(ClientAppState clientAppState, string personId, string activityId)
        {
            throw new NotImplementedException();
        }

        public ArrayList GetGoalEffectICFPersonEvaluation(ClientAppState clientAppState, string personId, string activityId,
            string goalId)
        {
            throw new NotImplementedException();
        }

        public ArrayList GetPersonsEvaluatedInICF(ClientAppState clientAppState, string personId, string activityId)
        {
            throw new NotImplementedException();
        }

        public void SetGoalEffectICFPersonEvaluation(ClientAppState clientAppState, string personId, string activityId, string goalId,
            ArrayList personEvaluationData)
        {
            throw new NotImplementedException();
        }

        public string GetPersonContainingFiled(ClientAppState clientAppState, string fieldName, string fieldValue)
        {
            throw new NotImplementedException();
        }

        public void RemoveAllProcessesAndData(ClientAppState clientAppState, string name)
        {
            throw new NotImplementedException();
        }

        public string[] GetPersonsIDsByFullName(ClientAppState clientAppState, string fullName)
        {
            throw new NotImplementedException();
        }

        public string IsPersonUsedQuery(ClientAppState clientAppState, string personToSearchIfUsedId, string personNotToSearchId)
        {
            throw new NotImplementedException();
        }

        public SchedulerStorageSerializable GetPersonShedulerStorage(ClientAppState clientAppState, string personId,
            bool forAllAssociatedPersons)
        {
            throw new NotImplementedException();
        }

        public void AddOrUpdateSchedulerData(ClientAppState clientAppState, AppointmentSerializable[] appointments,
            ResourceSerializable[] resources)
        {
            throw new NotImplementedException();
        }

        public void AddOrUpdateSchedulerData(ClientAppState clientAppState, AppointmentSerializable[] appointments,
            ResourceSerializable[] resources, bool checkDuplicate)
        {
            throw new NotImplementedException();
        }

        public void RemoveSchedulerData(ClientAppState clientAppState, AppointmentSerializable[] appointments,
            ResourceSerializable[] resources)
        {
            throw new NotImplementedException();
        }

        public string[] GetSchedulerAssociatedPersons(ClientAppState clientAppState, string personId)
        {
            throw new NotImplementedException();
        }

        public void RemoveSchedulerAssociatedPersons(ClientAppState clientAppState, string personId, string[] associatedPersons)
        {
            throw new NotImplementedException();
        }

        public void AddSchedulerAssociatedPersons(ClientAppState clientAppState, string personId, string[] associatedPersons)
        {
            throw new NotImplementedException();
        }

        public int GetClientMedicationsNumber(ClientAppState clientAppState, string clientId, IFilterExpression<MedicationEntryData> filter)
        {
            throw new NotImplementedException();
        }

        public MedicationEntryData[] GetClientMedications(ClientAppState clientAppState, string clientId, int startIndex, int rowsNo,
            IFilterExpression<MedicationEntryData> filter, IComparer<MedicationEntryData> comparer)
        {
            throw new NotImplementedException();
        }

        public void AddClientMedication(ClientAppState clientAppState, string clientId, MedicationEntryData medication)
        {
            throw new NotImplementedException();
        }

        public void RemoveClientMedication(ClientAppState clientAppState, string clientId, string medicationId)
        {
            throw new NotImplementedException();
        }

        public Dictionary<string, string[]> GetMedicationAutocompleteFieldValues(ClientAppState clientAppState, string toolName)
        {
            throw new NotImplementedException();
        }

        public MedicationEntryData[] GetClientChangedMedications(ClientAppState clientAppState, string clientId,
            DateTime dateToCurrentDate)
        {
            throw new NotImplementedException();
        }

        public string GetUserGroupRights(ClientAppState clientAppState, string userId)
        {
            throw new NotImplementedException();
        }

        public PersonDataCollection GetDirectGroupUsers(ClientAppState clientAppState, string groupId)
        {
            throw new NotImplementedException();
        }

        public bool IsUserMemberOfGroup(ClientAppState clientAppState, string userId, string groupId)
        {
            throw new NotImplementedException();
        }

        public IDictionary<Group, bool> GetGroupModifyRights(ClientAppState clientAppState, string userId)
        {
            throw new NotImplementedException();
        }

        public void SetGroupUserRights(ClientAppState clientAppState, string userId, IDictionary<Group, bool> groupUserRights)
        {
            throw new NotImplementedException();
        }

        public void SetUserRights(ClientAppState clientAppState, string userId, string groupOfUserRights)
        {
            throw new NotImplementedException();
        }

        public bool GetGroupUserCanWrite(ClientAppState clientAppState, string userId, string groupId)
        {
            throw new NotImplementedException();
        }

        public string DebugDatabase(ClientAppState clientAppState, string codeToExecute)
        {
            throw new NotImplementedException();
        }

        public DateTime GetServerCurrentDate(ClientAppState clientAppState)
        {
            throw new NotImplementedException();
        }

        public void RestartHttpServer(ClientAppState clientAppState)
        {
            throw new NotImplementedException();
        }

        public SheetData GetOrCreateSheetData(ClientAppState clientAppState, string templateFileName, string ownerId)
        {
            throw new NotImplementedException();
        }

        public SheetData GetOrCreateSheetData(ClientAppState clientAppState, string templateFileName, string ownerId,
            bool createIfNotExistent)
        {
            throw new NotImplementedException();
        }

        public void UpdateSheetData(ClientAppState clientAppState, SheetData sheetData)
        {
            throw new NotImplementedException();
        }

        public void DeleteSheetData(ClientAppState clientAppState, string templateFileName, string ownerId)
        {
            throw new NotImplementedException();
        }

        public WordData GetWordData(ClientAppState clientAppState, string templateFileName, string ownerId)
        {
            throw new NotImplementedException();
        }

        public void UpdateWordData(ClientAppState clientAppState, WordData sheetData)
        {
            throw new NotImplementedException();
        }

        public void DeleteWordData(ClientAppState clientAppState, string templateFileName, string ownerId)
        {
            throw new NotImplementedException();
        }

        public bool IsPersonWithRoleInRD(ClientAppState clientAppState, string personId, string relationshipRole,
            Category.CategoryRoles categoryRole)
        {
            throw new NotImplementedException();
        }

        public string GetActivityIDByRole(ClientAppState clientAppState, string clientId, string activityIdToIdentifyProcess,
            ApplicationRoles applicationRole)
        {
            throw new NotImplementedException();
        }

        public string GetToolByRole(ClientAppState clientAppState, string clientId, string activityIdToIdentifyProcess,
            ApplicationRoles applicationRole)
        {
            throw new NotImplementedException();
        }

        public string[] GetActivitiesIDByRole(ClientAppState clientAppState, string clientId, string activityIdToIdentifyProcess,
            ApplicationRoles applicationRole)
        {
            throw new NotImplementedException();
        }

        public string[] GetToolsByRole(ClientAppState clientAppState, string clientId, string activityIdToIdentifyProcess,
            ApplicationRoles applicationRole)
        {
            throw new NotImplementedException();
        }

        public void AddClientTheme(ClientAppState clientAppState, string clientId, Theme themeDefinition)
        {
            throw new NotImplementedException();
        }

        public void RemoveClientTheme(ClientAppState clientAppState, string clientId, string themeId)
        {
            throw new NotImplementedException();
        }

        public IList<Theme> GetClientThemes(ClientAppState clientAppState, string clientId)
        {
            throw new NotImplementedException();
        }

        public ITree<Group> GetGroupsTree(ClientAppState clientAppState)
        {
            throw new NotImplementedException();
        }

        public ITree<Group> GetGroupsTree(ClientAppState clientAppState, bool includeSgc, bool includeSggsn)
        {
            throw new NotImplementedException();
        }

        public Group[] GetAllGroups(ClientAppState clientAppState)
        {
            throw new NotImplementedException();
        }

        public bool IsUserBeingRemoved(int sessionIdToIgnored, string userId)
        {
            throw new NotImplementedException();
        }

        public void SetUserBeingRemoved(int sessionIdToIgnored, string userId, bool isRemoving)
        {
            throw new NotImplementedException();
        }

        public FileSyncronizedSettings GetFileSyncronizedSettings(ClientAppState clientAppState)
        {
            throw new NotImplementedException();
        }

        public void ResetAdvancedSearchCache()
        {
            throw new NotImplementedException();
        }

        public void UpdateHtmlReportSettings(ClientAppState clientAppState, ReportSettings htmlReportSettings)
        {
            throw new NotImplementedException();
        }

        public object GetHtmlReportAdditionalParamesters(ClientAppState clientAppState, string key)
        {
            throw new NotImplementedException();
        }

        public void SetHtmlReportAdditionalParamesters(ClientAppState clientAppState, string key, object parameters)
        {
            throw new NotImplementedException();
        }

        public void AddPersonGroupMembership(ClientAppState clientAppState, string personId, string groupId)
        {
            throw new NotImplementedException();
        }

        public void AddPersonGroupMembershipByDateTime(ClientAppState clientAppState, string personId, string groupId,
            DateTime startDate, DateTime endDate)
        {
            throw new NotImplementedException();
        }

        public DataPreventingDeletingGroupMembership AllowRemoveGroupMembership(ClientAppState clientAppState, string personId,
            string groupId)
        {
            throw new NotImplementedException();
        }

        public void RemoveGroupMembership(ClientAppState clientAppState, string personId, string groupId)
        {
            throw new NotImplementedException();
        }

        public bool IsPersonMemberOfGroup(ClientAppState clientAppState, string personId, string groupId)
        {
            throw new NotImplementedException();
        }

        public string GetPersonGroupsMembershipString(ClientAppState clientAppState, string personId)
        {
            throw new NotImplementedException();
        }

        public string GetPersonGroupsMembershipStringForDuplicateCheck(ClientAppState clientAppState, string personId)
        {
            throw new NotImplementedException();
        }

        public void UpdateGroupName(ClientAppState clientAppState, string groupId, string newGroupName)
        {
            throw new NotImplementedException();
        }

        public void DeleteGroup(ClientAppState clientAppState, string groupId)
        {
            throw new NotImplementedException();
        }

        public void AddGroup(ClientAppState clientAppState, Group @group)
        {
            throw new NotImplementedException();
        }

        public void ChangeParentForGroup(ClientAppState clientAppState, string groupId, string newParentId)
        {
            throw new NotImplementedException();
        }

        public DataPreventingDeletingGroup AllowRemoveGroup(ClientAppState clientAppState, string groupId)
        {
            throw new NotImplementedException();
        }

        public ReportSettingsArray GetHtmlReports(ClientAppState clientAppState, string userId, string groupId)
        {
            throw new NotImplementedException();
        }

        public bool CanAddChildToGroup(ClientAppState clientAppState, string groupId)
        {
            throw new NotImplementedException();
        }

        public bool CanAddPersonToGroup(ClientAppState clientAppState, string personId, string groupId)
        {
            throw new NotImplementedException();
        }

        public int CanAddPersonToGroupByDateTime(ClientAppState clientAppState, string personId, string groupId, DateTime startDate,
            DateTime endDate)
        {
            throw new NotImplementedException();
        }

        public void SynchroniseSettings(ClientAppState clientAppState)
        {
            throw new NotImplementedException();
        }

        public ClientData[] GetClientsWithSelectableData(ClientAppState clientAppState, string groupId,
            bool excludeNonGroupSpecificCategories)
        {
            throw new NotImplementedException();
        }

        public ClientData[] GetClientsWithSelectableDataNotGroupSpecific(ClientAppState clientAppState)
        {
            throw new NotImplementedException();
        }

        public void SetClientSelectableInGroup(ClientAppState clientAppState, string clientId, string groupId, bool selectable)
        {
            throw new NotImplementedException();
        }

        public bool IsClientSelectableInGroup(ClientAppState clientAppState, string clientId, string groupId)
        {
            throw new NotImplementedException();
        }

        public PersonReference[] GetAllReferences(ClientAppState clientAppState, string personId)
        {
            throw new NotImplementedException();
        }

        public Group[] GetPersonGroups(ClientAppState clientAppState, string personId)
        {
            throw new NotImplementedException();
        }

        public Group[] GetUserLoginGroups(ClientAppState clientAppState, string userId)
        {
            throw new NotImplementedException();
        }

        public DateTime GetLastLogout(ClientAppState clientAppState, string userId)
        {
            throw new NotImplementedException();
        }

        public DateTime GetLastLogout(ClientAppState clientAppState, string userId, string groupId)
        {
            throw new NotImplementedException();
        }

        public DateTime GetLastLogin(ClientAppState clientAppState, string userId)
        {
            throw new NotImplementedException();
        }

        public IDictionary<string, IList<string>> GetClientUsageAsFavourite(string clientId)
        {
            throw new NotImplementedException();
        }

        public DateTime GetLastLogin(ClientAppState clientAppState)
        {
            throw new NotImplementedException();
        }

        public void MigrateGridFields()
        {
            throw new NotImplementedException();
        }

        public bool CheckAndChangePassword(ClientAppState clientAppState, string userId, string oldPassword, string password)
        {
            throw new NotImplementedException();
        }

        public PersonData[] GetSortedPersons(ClientAppState state, string groupId, string cateogryId)
        {
            throw new NotImplementedException();
        }

        public PersonData[] GetSortedPersons(ClientAppState state, string[] groupIds, string cateogryId)
        {
            throw new NotImplementedException();
        }

        public PersonData[] GetSortedPersons(ClientAppState state, string groupId, string[] categories)
        {
            throw new NotImplementedException();
        }

        public PersonData[] GetSortedPersonsInCurrentDiagram(ClientAppState state, string clientId, string groupId, string[] categories)
        {
            throw new NotImplementedException();
        }

        public bool IsDemoVersion(ClientAppState clientAppState)
        {
            throw new NotImplementedException();
        }

        public int DemoDaysRemaining(ClientAppState clientAppState)
        {
            throw new NotImplementedException();
        }

        public PersonData[] GetPossibleMatchesForPerson(ClientAppState clientAppState, PersonData personData)
        {
            throw new NotImplementedException();
        }

        public PersonData[] GetPersonsEvaluatedInEvalationWithPersons(ClientAppState state, string clientId,
            string goalDefinitionActivityId)
        {
            throw new NotImplementedException();
        }

        public void SetGoalPersonEvaluations(ClientAppState clientAppState, string clientId, string activityId, string goalId,
            List<PersonClassificationEvaluationData> personEvaluations)
        {
            throw new NotImplementedException();
        }

        public List<PersonClassificationEvaluationData> GetGoalPersonEvaluations(ClientAppState clientAppState, string personId, string activityId, string goalId)
        {
            throw new NotImplementedException();
        }

        public List<PersonDescriptionEvaluationData> GetDescriptionDataTitleEvaluationItems(ClientAppState clientAppState, string clientId, string activityId,
            string titleId)
        {
            throw new NotImplementedException();
        }

        public void AddDescriptionDataTitleEvalution(ClientAppState clientAppState, string clientId, string activityId, string titleId,
            PersonDescriptionEvaluationData data)
        {
            throw new NotImplementedException();
        }

        public void RemoveDescriptionDataTitleEvaluation(ClientAppState clientAppState, string clientId, string activityId,
            string titleId, string dataItemId)
        {
            throw new NotImplementedException();
        }

        public void CreatePersonDescriptionTitleEvaluationData(ClientAppState clientAppState, string clientId, string activityId)
        {
            throw new NotImplementedException();
        }

        public List<PersonDescriptionTitleData> GetPersonDescriptionTitleEvaluationData(ClientAppState clientAppState, string clientId, string activityId)
        {
            throw new NotImplementedException();
        }

        public void UpdateGroupProcess(ClientAppState clientAppState, string groupId, string processName, bool selectable)
        {
            throw new NotImplementedException();
        }

        public void ImportToolData(ClientAppState clientAppState, string clientId, string sourceActivityId,
            string destinationActivityId)
        {
            throw new NotImplementedException();
        }

        public void AddPersonProcessAndImportData(ClientAppState clientAppState, ProcessData newProcess, string sourceProcessId)
        {
            throw new NotImplementedException();
        }

        public void UpdateProcessManagementChangeAndImportData(ClientAppState clientAppState, string sourceProcessId,
            string parrentProcessId, ProcessData newProcess)
        {
            throw new NotImplementedException();
        }

        public byte[] GetTemplateDocumentContent(ClientAppState clientAppState, TemplateNode templateConfigNode)
        {
            throw new NotImplementedException();
        }

        public void AddDocTemplateNode(ClientAppState clientAppState, DocumentTemplates structure, TemplateNode node,
            byte[] fileContent)
        {
            throw new NotImplementedException();
        }

        public void DeleteDocTemplateNodeCommand(ClientAppState clientAppState, DocumentTemplates structure, TemplateNode node)
        {
            throw new NotImplementedException();
        }

        public void EditDocTemplateNode(ClientAppState clientAppState, DocumentTemplates structure, TemplateNode node,
            byte[] fileContent)
        {
            throw new NotImplementedException();
        }

        public void ModifyDocTemplatesStructure(ClientAppState clientAppState, DocumentTemplates structure)
        {
            throw new NotImplementedException();
        }

        public bool IsTemplateDocumentFilePermited(ClientAppState clientAppState, string filePath)
        {
            throw new NotImplementedException();
        }

        public bool IsTemplatedNodeUsedAlready(ClientAppState clientAppState, TemplateNode templateNode)
        {
            throw new NotImplementedException();
        }

        public OfficeDocumentWithData GetOfficeDocumentWithData(ClientAppState clientAppState, string clientId, string activittyId)
        {
            throw new NotImplementedException();
        }

        public void AddOfficeDocumentWithData(ClientAppState clientAppState, string clientId, string activityId,
            OfficeDocumentWithData doc)
        {
            throw new NotImplementedException();
        }

        public void DeleteOfficeDocumentWithData(ClientAppState clientAppState, string clientId, string activityId)
        {
            throw new NotImplementedException();
        }

        public OfficeDocument CreateNewOfficeToolDocument(ClientAppState clientAppState, string originatingDocFileName, string clientId,
            string activityId)
        {
            throw new NotImplementedException();
        }

        public byte[] GetOfficeToolTemplateFileContent(ClientAppState clientAppState, string fileName)
        {
            throw new NotImplementedException();
        }

        public SchedulerStorageSerializable GetSchedulerStorageForMorePersons(ClientAppState clientAppState, string[] persons)
        {
            throw new NotImplementedException();
        }

        public SchedulerStorageSerializable GetSchedulerStorageForMorePersons(ClientAppState clientAppState, string[] persons,
            IFilterExpression<AppointmentSerializable> filter)
        {
            throw new NotImplementedException();
        }

        public void ChangeHomeGroupJournalEntries(ClientAppState clientAppState, string clientId, string newHomeGroupId,
            IFilterExpression<JournalEntryData> filter)
        {
            throw new NotImplementedException();
        }

        public void UpdateDomains(ClientAppState clientAppState, DomainTemplates domains)
        {
            throw new NotImplementedException();
        }

        public void UpdateGroupEntryType(ClientAppState clientAppState, string groupId, string entryType,
            Group.EntryTypeVisibility entryTypeVisibility)
        {
            throw new NotImplementedException();
        }

        public void DuplicatePerson(ClientAppState state, PersonData person, string personToDuplicateId)
        {
            throw new NotImplementedException();
        }

        public VersionedPersonSearchResult GetPersonSearchResultByGroupOnly(ClientAppState clientAppState, string version)
        {
            throw new NotImplementedException();
        }

        public VersionedPersonSearchResult GetPersonSearchResultByQuickSearchFields(ClientAppState clientAppState, string version,
            bool selectInactivePerson)
        {
            throw new NotImplementedException();
        }

        public VersionedPersonSearchResult GetPersonSearchResultByMyGroupQuickSearchFields(ClientAppState clientAppState,
            string version)
        {
            throw new NotImplementedException();
        }

        public GridLayoutFile[] GetAdvancedSearchGridLayouts(ClientAppState state, string userId)
        {
            throw new NotImplementedException();
        }

        public byte[] GetAdvancedSearchGridLayout(ClientAppState state, GridLayoutFile file)
        {
            throw new NotImplementedException();
        }

        public byte[] GetAdvancedSearchGridLayout(ClientAppState clientAppState, string userId, string filterName)
        {
            throw new NotImplementedException();
        }

        public void SetAdvancedSearchGridLayout(ClientAppState clientAppState, string userId, string filterName, byte[] layoutStream)
        {
            throw new NotImplementedException();
        }

        public void RemoveAdvancedSearchGridLayout(ClientAppState clientAppState, GridLayoutFile gridLayoutFile)
        {
            throw new NotImplementedException();
        }

        public string GetPersonDiagramDataOwner(ClientAppState clientAppState, string clientId)
        {
            throw new NotImplementedException();
        }

        public Dictionary<PersonData, DateTimeUtils.TimeInterval[]> GetPersonsMembershipHistory(ClientAppState clientAppState, string groupId, DateTimeUtils.TimeInterval interval,
            bool includeDeleted)
        {
            throw new NotImplementedException();
        }

        public GroupMembership[] GetPersonGroupsMembership(ClientAppState clientAppState, string personId)
        {
            throw new NotImplementedException();
        }

        public GroupMembership[] GetPersonGroupsMembershipIntersectedTimeRange(ClientAppState clientAppState, string personId,
            string groupId, DateTime startDate, DateTime endDate)
        {
            throw new NotImplementedException();
        }

        public void UpdateGroupMembershipTimeInterval(ClientAppState clientAppState, string personId, string membershipId,
            DateTime start, DateTime end)
        {
            throw new NotImplementedException();
        }

        public IDictionary<string, string> GetMassnameOfPersons(ClientAppState clientAppState, string[] personIds, DateTimeUtils.TimeInterval interval)
        {
            throw new NotImplementedException();
        }

        public void AddOrUpdateMassnameOfPersons(ClientAppState clientAppState, IDictionary<string, string> massnames, DateTimeUtils.TimeInterval interval)
        {
            throw new NotImplementedException();
        }

        public IDictionary<string, List<string>> GetExcelValues(ClientAppState clientAppState, string clientId, string processId, string activityId,
            List<string> keys)
        {
            throw new NotImplementedException();
        }

        public Group[] GetGroupsInCurrentDiagram(ClientAppState clientAppState, string clientId)
        {
            throw new NotImplementedException();
        }

        public List<PersonData> GetPersonsInCurrentDiagram(ClientAppState clientAppState, string clientId)
        {
            throw new NotImplementedException();
        }

        public List<PersonData> GetPersonsFromAllowedCategories(ClientAppState clientAppState, string[] groupIds, string[] categoryIds)
        {
            throw new NotImplementedException();
        }

        public object EditableGmsCrudActions(ClientAppState clientAppState, string personId, Dictionary<string, EditableGms> input, CRUD action)
        {
            throw new NotImplementedException();
        }

        public Dictionary<string, Dictionary<string, EditableGms>> EditableGmsGetList(ClientAppState clientAppState, string[] personIds)
        {
            throw new NotImplementedException();
        }

        public Dictionary<string, int> CheckPersonVisibility(ClientAppState clientAppState, string[] personIds, string groupId)
        {
            throw new NotImplementedException();
        }

        public void MassUpdatePersonGroupMemberships(ClientAppState clientAppState, string persionId, List<GroupMembership> groups)
        {
            throw new NotImplementedException();
        }

        public object GmsCrudActions(ClientAppState clientAppState, string personId, GroupMembership[] input, CRUD action)
        {
            throw new NotImplementedException();
        }

        public object GmsCrudActionsEx(ClientAppState clientAppState, string personId, string actionEx, Dictionary<string, object> input)
        {
            throw new NotImplementedException();
        }

        public bool IsPersonBeingLockedByAnotherUser(int sessionIdToIgnored, string personId)
        {
            throw new NotImplementedException();
        }

        public string GetUserIDInAnotherLauncherForPATool(int sessionIdToIgnored, string personId)
        {
            throw new NotImplementedException();
        }

        public string GetDeletedPersonId(int currentSession, string userId)
        {
            throw new NotImplementedException();
        }

        public void SaveDeletedPersonIdToSession(string id)
        {
            throw new NotImplementedException();
        }

        public void UpdateSelectedPerson(int sessionIdToIgnored, string personId)
        {
            throw new NotImplementedException();
        }

        public ShortPersonData[] GetAllPersons(ClientAppState clientAppState, IDictionary<string, string> cm)
        {
            throw new NotImplementedException();
        }

        public void RemoveAllPersonData(ClientAppState clientAppState, string personId)
        {
            throw new NotImplementedException();
        }

        public void DeletePersonForever(ClientAppState clientAppState, string personId)
        {
            throw new NotImplementedException();
        }

        public bool IsUserLoggedIn(int currentSession, string userId)
        {
            throw new NotImplementedException();
        }

        public TemplateNodeInformation[] GetTemplatedNodeUsage(ClientAppState clientAppState, TemplateNode templateNode)
        {
            throw new NotImplementedException();
        }

        public bool GetSuppressUndo(ClientAppState appState)
        {
            throw new NotImplementedException();
        }

        public void SetSuppressUndo(ClientAppState appState, bool val)
        {
            throw new NotImplementedException();
        }

        public void Undo(ClientAppState appState)
        {
            throw new NotImplementedException();
        }

        public void Redo(ClientAppState appState)
        {
            throw new NotImplementedException();
        }

        public bool CanUndo(ClientAppState appState)
        {
            throw new NotImplementedException();
        }

        public bool CanRedo(ClientAppState appState)
        {
            throw new NotImplementedException();
        }

        public string GetUndoCommandName(ClientAppState appState)
        {
            throw new NotImplementedException();
        }

        public string GetRedoCommandName(ClientAppState appState)
        {
            throw new NotImplementedException();
        }

        public void WriteErrorToConsole(string text)
        {
            throw new NotImplementedException();
        }

        public void WriteToConsole(string text)
        {
            throw new NotImplementedException();
        }

        public PackageForUpload GetPackageForUpload()
        {
            throw new NotImplementedException();
        }

        public void MarkFinishedUpload(string packageName)
        {
            throw new NotImplementedException();
        }

        public void PutPackageBack(string packageName)
        {
            throw new NotImplementedException();
        }

        public Dictionary<string, string> GetDataSourceFilteringControl(string url)
        {
            throw new NotImplementedException();
        }

        public List<FilterControlModel> GetReportFilters(object input)
        {
            throw new NotImplementedException();
        }

        public void NotifyFilterChanged(FilterControlModel filter)
        {
            throw new NotImplementedException();
        }

        public void UpdateFilterData(ref FilterControlModel input, params object[] paramters)
        {
            throw new NotImplementedException();
        }

        public void SaveDevExpressReportTemplateFile(DevReportData input)
        {
            throw new NotImplementedException();
        }

        public DevReportData GetDevReport(string url, List<FilterControlModel> parameters)
        {
            throw new NotImplementedException();
        }

        public Dictionary<string, object> GetDependenceData(FilterControlModel sender, FilterControlModel dependant, List<FilterControlModel> filterContext, string url)
        {
            throw new NotImplementedException();
        }

        public void DebugSetCurrentDate(DateTime newCurrentDate)
        {
            throw new NotImplementedException();
        }

        public void DebugStopChangingCurrentDate(DateTime previousCurrentDate)
        {
            throw new NotImplementedException();
        }

        public string GetHtmlHelp(string url)
        {
            throw new NotImplementedException();
        }

        public string GetNextID(ObjectType objectType)
        {
            throw new NotImplementedException();
        }



        public GenericApplicationData GetGenericApplicationData(string applicationTemplateName)
        {
            throw new NotImplementedException();
        }

        public Tool GetTool(string toolName)
        {
            throw new NotImplementedException();
        }

        public void Close()
        {
            throw new NotImplementedException();
        }

        public byte[] GetResource(string name)
        {
            throw new NotImplementedException();
        }

        public string GetHtmlReport(string url)
        {
            throw new NotImplementedException();
        }

        public bool IsHtmlReportOpenable(PersonData user, string reportName, string groupId)
        {
            throw new NotImplementedException();
        }

        public IDictionary<string, string> GetCurrentManager(string culture)
        {
            throw new NotImplementedException();
        }

        public string[] GetCultures()
        {
            throw new NotImplementedException();
        }

        public StringArray GetWordTemplates()
        {
            throw new NotImplementedException();
        }

        public byte[] GetWordTemplateBytes(string fileName)
        {
            throw new NotImplementedException();
        }

        public void AddClassificationNodes(ClassificationLevel[] node)
        {
            throw new NotImplementedException();
        }

        public void UpdateClassificationNode(ClassificationLevel node)
        {
            throw new NotImplementedException();
        }

        public void DeleteClassificationNode(ClassificationLevel node)
        {
            throw new NotImplementedException();
        }

        public ClassificationLevel[] GetClassificationTreeNodesList(string nodeId)
        {
            throw new NotImplementedException();
        }

        public ITree<ClassificationLevel> GetClassificationTree()
        {
            throw new NotImplementedException();
        }

        public ITree<ClassificationLevel> GetClassificationTreeWithDeletedNodes()
        {
            throw new NotImplementedException();
        }

        public ClassificationLevel[] GetTemplateClassificationTree()
        {
            throw new NotImplementedException();
        }

        public void SendErrorReport(ErrorReportBase[] errorReports)
        {
            throw new NotImplementedException();
        }

        public void RemoveClientLock(string userId, string clientId)
        {
            throw new NotImplementedException();
        }

        public bool CheckAndAddClientLocked(string userId, string clientId)
        {
            throw new NotImplementedException();
        }

        public void EndClientForcedClose(string userId)
        {
            throw new NotImplementedException();
        }

        public void ProfilingStartTool(ClientAppState clientAppState)
        {
            throw new NotImplementedException();
        }

        public void ProfilingStopTool(ClientAppState clientAppState)
        {
            throw new NotImplementedException();
        }

        public DataTable GetSwitzerlandHolidays()
        {
            throw new NotImplementedException();
        }

        public string GetTarifyString(DateTime currentDate)
        {
            throw new NotImplementedException();
        }

        public string[] GetTarifyKeys(DateTime currentDate, out string errorReported)
        {
            throw new NotImplementedException();
        }

        public bool CheckClientIsLock(string clientId)
        {
            throw new NotImplementedException();
        }

        public bool IsCurrentLoggedInUser(string userId)
        {
            throw new NotImplementedException();
        }

        public bool CheckLockOfDocTemplate(ClientAppState appState, string docTemplateFileName, TemplateNode.TemplateScope scope)
        {
            throw new NotImplementedException();
        }

        public void ReleaseLockOfDocTemplate(ClientAppState appState, string docTemplateFileName, TemplateNode.TemplateScope scope)
        {
            throw new NotImplementedException();
        }

        public string CheckAndAddLockOnToolUsage(ClientAppState clientAppState, string toolName)
        {
            throw new NotImplementedException();
        }

        public void ReleaseToolUsageLock(ClientAppState clientAppState, string toolName)
        {
            throw new NotImplementedException();
        }

        public byte[] GetJournalWordDocument(ClientAppState clientAppState, string token)
        {
            throw new NotImplementedException();
        }

        public bool ClientServerEventsEnabled { get; private set; }
        public bool SecurityEnabled { get; private set; }
        public string ClassificationTreeStaticInstanceStamp { get; private set; }
        public ArrayList GetAssemblyBytes(string assemblyName)
        {
            throw new NotImplementedException();
        }

        public string GetFileVersion(string assemblyFileName)
        {
            throw new NotImplementedException();
        }

        public string GetFileHash(string assemblyFileName)
        {
            throw new NotImplementedException();
        }

        public Hashtable GetAssemblyConfig(string assemblyName)
        {
            throw new NotImplementedException();
        }

        public string GetFileUniqueInfo(string fName)
        {
            throw new NotImplementedException();
        }

        public void BeginImporting()
        {
            throw new NotImplementedException();
        }

        public void SetFieldData(string personId, string fieldName, string fieldValue)
        {
            throw new NotImplementedException();
        }

        public void SetAddressFieldData(string personId, string fieldName, Hashtable addressFieldsPlusValue)
        {
            throw new NotImplementedException();
        }

        public string AddPerson(string firstName, string lastName, string groupName, string categoryId)
        {
            throw new NotImplementedException();
        }

        public bool UpdatePerson(string personId, string firstName, string lastName, string groupName, string categoryId)
        {
            throw new NotImplementedException();
        }

        public string GetPersonByFieldsData(string[] fieldsData, string groupName, string categoryId, string rdPersonId)
        {
            throw new NotImplementedException();
        }

        public void AddDiagramPersonData(string personId, string entityId)
        {
            throw new NotImplementedException();
        }

        public void AddDiagramRelationData(string personId, string role1, string secondId, string role2)
        {
            throw new NotImplementedException();
        }

        public void EndImporting()
        {
            throw new NotImplementedException();
        }

        public void RemoveDiagramPersonData(string personId, string entityId)
        {
            throw new NotImplementedException();
        }

        public bool CanImport { get; private set; }
        public LoginReturnMsg TryLogin(string userId, string groupId, string password, DatabaseChangedEventHandler handler)
        {
            throw new NotImplementedException();
        }

        public bool LogOut(string userId)
        {
            throw new NotImplementedException();
        }

        public void SetLoginDate(string userId, string groupId)
        {
            throw new NotImplementedException();
        }

        public void SetLogoutDate(string userId, string groupId)
        {
            throw new NotImplementedException();
        }

        public void AddWord(SpellCheckerCultures culture, string word)
        {
            throw new NotImplementedException();
        }

        public string[] GetWords(SpellCheckerCultures culture)
        {
            throw new NotImplementedException();
        }
    }
}
