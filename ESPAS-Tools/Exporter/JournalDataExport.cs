﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using ApplicationUtilities.Utils;
using DataDomain.ApplicationDefinition;
using DataDomain.DataObjects;
using DataDomain.Utils;
using Exporter.Helpers;
using ServerClientExchange.Interfaces;

namespace Exporter
{
    /// <summary>
    /// Class for exporting person journal data. An individual person journal data will be exported 
    /// to a HTML file. All HTML file will be saved to location which its path is input in App.config file.
    /// </summary>
    public class JournalDataExport : DataExportBase
    {
        public const string JounalReportName = "Journal_LinqStartFromJournalApp.rep";
        private readonly int _numberOfConcurrentThread = 1;

        private string _outputLocation;
        private ApplicationDefinitionData _appDefinitionData;
        
        private IDictionary<string, GroupMembership> _clientGroupMemberships;
        private ManualResetEvent _wait;

        private IDictionary<string, PersonData> _persons;
        public IDictionary<string, PersonData> Persons
        {
            get
            {
                if (_persons == null || _persons.Count == 0)
                    _persons = PromovaHelper.GetPersons(Dispatcher, ClientAppState);

                return _persons;
            }
            set { _persons = value; }
        }

        private IDictionary<string, GenericPerson> _allClients;
        public IDictionary<string, GenericPerson> Clients
        {
            get { return _allClients; }
        }

        public override string OutputLocation
        {
            get { return _outputLocation; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    var path = Path.Combine(Environment.CurrentDirectory, "Journal");
                    _outputLocation = path;
                }
                else
                {
                    _outputLocation = value;
                }

                if (!Directory.Exists(_outputLocation))
                {
                    Directory.CreateDirectory(_outputLocation);
                }
            }
        }

        /// <summary>
        /// Create an instance of <see cref="JournalDataExport"/> class.
        /// </summary>
        /// <param name="dispatcher">The dispatcher which consist of proxies to the PROmova server.</param>
        /// <param name="clientAppState">The client state</param>
        /// /// <param name="numberOfConcurrentThread">The client state</param>
        public JournalDataExport(ServerDispatcher dispatcher, ClientAppState clientAppState, int numberOfConcurrentThread)
            : base(dispatcher, clientAppState)
        {
            _allClients = new Dictionary<string, GenericPerson>();
            _clientGroupMemberships = new Dictionary<string, GroupMembership>();
            _numberOfConcurrentThread = numberOfConcurrentThread;
        }

        /// <summary>
        /// Gets the application definition data
        /// </summary>
        private void GetApplicationDefinitionData()
        {
            _appDefinitionData = Dispatcher.ServerLogic.GetApplicationDefinitionData();
        }

        private ServerDispatcher CreateServerDispatcher(AppSetting appSetting)
        {
            string url = string.Format("tcp://{0}:{1}/{2}",
                appSetting.ServerUrl,
                appSetting.ServerPort,
                appSetting.RemotePublishedObject);

            var proxyGenericServer = Activator.GetObject(typeof(IGenericServer), url) as IGenericServer;
            return new ServerDispatcher(proxyGenericServer);
        }

        /// <summary>
        /// Gets clients who are still active
        /// </summary>
        private void GetActiveClients()
        {
            var collections = new List<PersonData>[_numberOfConcurrentThread];
            int indexer = 0;
            foreach (var entry in Persons)
            {
                if (collections[indexer % _numberOfConcurrentThread] == null)
                    collections[indexer % _numberOfConcurrentThread] = new List<PersonData>();

                collections[indexer % _numberOfConcurrentThread].Add(entry.Value);
                indexer++;
            }

            var appSetting = new AppSetting(ConfigurationManager.AppSettings);
            var dispatchers = new ServerDispatcher[_numberOfConcurrentThread];
            for (int index = 0; index < _numberOfConcurrentThread; index++)
            {
                var dispatcher = CreateServerDispatcher(appSetting);
                dispatchers[index] = dispatcher;
            }

            var allClients = new List<IDictionary<string, GenericPerson>>();
            var tasks = collections.Select(pc => Task.Factory.StartNew(() =>
            {
                int counter = 0;
                var clientDic = new Dictionary<string, GenericPerson>();
                foreach (var p in pc)
                {
                    var personId = p.ID;
                    var genericPerson = dispatchers[counter % _numberOfConcurrentThread].PersonBusinessLogic.GetGenericPerson(personId);

                    if ((genericPerson is IClient) && (!clientDic.ContainsKey(personId)))
                    {
                        clientDic[personId] = genericPerson;
                    }
                    counter++;
                }
                return clientDic;
            })
            .ContinueWith(output => allClients.Add(output.Result))).ToArray();

            Task.WaitAll(tasks);

            foreach (IDictionary<string, GenericPerson> clientDic in allClients)
            {
                foreach (KeyValuePair<string, GenericPerson> pair in clientDic)
                {
                    if (!_allClients.ContainsKey(pair.Key))
                        _allClients[pair.Key] = pair.Value;

                    var membership = ((IClient)pair.Value).GroupsMembership;
                    var currentMembership = membership.FirstOrDefault(x => x.ValidFrom < DateTime.UtcNow && DateTime.UtcNow < x.ValidUntil) ??
                                            membership.OrderByDescending(x => x.ValidFrom).FirstOrDefault();
                    _clientGroupMemberships[pair.Key] = currentMembership;
                }
            }
        }

        /// <summary>
        /// Creates Url for the HML report
        /// </summary>
        /// <param name="personId">The person Id</param>
        /// <param name="userId">The current logged in user Id</param>
        /// <param name="groupId">The group Id</param>
        /// <param name="reportName">The report name</param>
        /// <returns></returns>
        private string CreateUrlReport(string personId, string userId, string groupId, string reportName)
        {
            string parameters = string.Format("{0}={1}&{2}={3}&{4}={5}&{6}={7}",
                HtmlReportsParameters.PERSON_ID, personId,
                HtmlReportsParameters.USER_ID, userId,
                HtmlReportsParameters.GROUP_ID, groupId,
                HtmlReportsParameters.CULTURE, "de-CH");

            return _appDefinitionData.HttpServerURL + "?" +
                   HtmlReportsParameters.REPORT_NAME + "=" + reportName + "&" +
                   parameters;
        }

        /// <summary>
        /// Creates output filename based on the client information
        /// </summary>
        /// <param name="personId">The client Id</param>
        /// <param name="fullName">The client fullname</param>
        /// <returns></returns>
        private string CreateFilePath(string personId, string fullName)
        {
            var fileName = string.Format("{0}-{1}.html", personId, fullName);

            var invalidChars = Path.GetInvalidFileNameChars();
            var fileNameWithInvalidCharsRemoved = new string(fileName.Where(x => !invalidChars.Contains(x)).ToArray());

            return Path.Combine(OutputLocation, fileNameWithInvalidCharsRemoved);
        }

        /// <summary>
        /// A callback to handle the response of web request
        /// </summary>
        /// <param name="asyncResult">The asynchronous result</param>
        private void GetResponseCallback(IAsyncResult asyncResult)
        {
            var state = asyncResult.AsyncState as RequestState;
            if (state == null)
                return;

            if (!asyncResult.IsCompleted)
                return;

            var response = state.Request.EndGetResponse(asyncResult);
            using (var stream = response.GetResponseStream())
            {
                if (stream == null)
                    return;

                var contentLength = (int)response.ContentLength;
                var fullName = _allClients[state.PersonId] != null ? _allClients[state.PersonId].FullName : "";
                var fileName = CreateFilePath(state.PersonId, fullName);

                using (var fs = File.OpenWrite(fileName))
                {
                    var content = new byte[contentLength];
                    int readBytes = stream.Read(content, 0, contentLength);
                    if (readBytes > 0)
                    {
                        fs.Write(content, 0, readBytes);
                        fs.Flush();
                    }
                }
            }

            // Signal to other threads to be available
            _wait.Set();
        }

        private void GetClientHtmlReport(string personId)
        {
            var reportUrl = CreateUrlReport(personId, ClientAppState.UserID,
                        _clientGroupMemberships[personId] != null ? _clientGroupMemberships[personId].GroupID : ClientAppState.GroupID,
                        JounalReportName);

            var request = (HttpWebRequest)WebRequest.Create(new Uri(reportUrl));
            request.Headers.Add(HttpRequestHeader.KeepAlive, "false");
            request.ProtocolVersion = HttpVersion.Version10;
            request.Timeout = 1000 * 60 * 15; // 15 minutes
            request.BeginGetResponse(GetResponseCallback, new RequestState { PersonId = personId, Request = request });
        }

        private void GetClientHtmlReportPls(string personId)
        {
            var groupId = _clientGroupMemberships[personId] != null ? _clientGroupMemberships[personId].GroupID : "" /*ClientAppState.GroupID*/;
            var reportUrl = CreateUrlReport(personId, ClientAppState.UserID, groupId, JounalReportName);

            var request = (HttpWebRequest)WebRequest.Create(new Uri(reportUrl));
            request.Headers.Add(HttpRequestHeader.KeepAlive, "false");
            request.ProtocolVersion = HttpVersion.Version10;
            request.Timeout = 1000 * 60 * 5; // 5 minutes
            request.ReadWriteTimeout = 1000 * 60 * 5; // 5 minutes

            var response = request.GetResponse();
            using (var stream = response.GetResponseStream())
            {
                if (stream == null)
                    return;

                var contentLength = (int)response.ContentLength;
                var fullName = _allClients[personId] != null ? _allClients[personId].FullName : "";
                var fileName = CreateFilePath(personId, fullName);

                using (var fs = File.OpenWrite(fileName))
                {
                    var content = new byte[contentLength];
                    int readBytes = stream.Read(content, 0, contentLength);
                    if (readBytes > 0)
                    {
                        fs.Write(content, 0, readBytes);
                        fs.Flush();
                    }
                }
            }
        }

        private int _totalPersons;
        private int _doneCounter = 0;

        private void IncreaseDoneCounter()
        {
            Interlocked.Increment(ref _doneCounter);
        }

        /// <summary>
        /// Exports persons journal data to HTML files which are stored in a location specified by the path input in the App.config file.
        /// </summary>
        public override void DoExport()
        {
            OnNotify("Getting application definition data...");
            GetApplicationDefinitionData();

            OnNotify("Getting all clients...");
            GetActiveClients();

            _totalPersons = _allClients.Count;
            _doneCounter = 0;

            OnNotify(string.Format("There are {0} cliens to be exporting...", _totalPersons));
            Thread.Sleep(100);
            if (_allClients != null)
            {
                foreach (var pair in _allClients)
                {
                    //_wait = new ManualResetEvent(false);

                    IncreaseDoneCounter();
                    OnPersonExported(new PersonExportEventArgs
                    {
                        Indexer = _doneCounter,
                        Total = _totalPersons,
                        PersonId = pair.Key,
                        PersonName = pair.Value.FullName
                    });

                    //OnNotify(string.Format("Journal entries of '{1} (ID = {0})' are exporting...", pair.Key, pair.Value.FullName));

                    //GetClientHtmlReport(pair.Key);
                    GetClientHtmlReportPls(pair.Key);

                    //_wait.WaitOne();
                }
            }
        }

        internal class RequestState
        {
            public WebRequest Request { get; set; }
            public string PersonId { get; set; }
        }
    }
}