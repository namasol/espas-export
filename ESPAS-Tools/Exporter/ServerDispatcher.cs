﻿using DataDomain.ServerClientExchange;
using ServerClientExchange.Interfaces;

namespace Exporter
{
    /// <summary>
    /// A dispatcher to the remoting server
    /// </summary>
    public class ServerDispatcher
    {
        private readonly IGenericServer _proxyRemoteServer;

        /// <summary>
        /// Gets the login logic
        /// </summary>
        public ILoginLogic LoginLogic
        {
            get { return _proxyRemoteServer; }
        }

        /// <summary>
        /// Gets the server logic
        /// </summary>
        public IServerLogic ServerLogic
        {
            get { return _proxyRemoteServer; }
        }

        /// <summary>
        /// Get the person business logic
        /// </summary>
        public IPersonBusinessLogic PersonBusinessLogic
        {
            get { return _proxyRemoteServer; }
        }

        /// <summary>
        /// Create an instance of <see cref="ServerDispatcher"/>
        /// </summary>
        /// <param name="proxyRemoteServer">The proxy of remoting server</param>
        public ServerDispatcher(IGenericServer proxyRemoteServer)
        {
            _proxyRemoteServer = proxyRemoteServer;
        }
    }
}
