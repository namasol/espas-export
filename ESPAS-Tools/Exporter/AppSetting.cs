﻿using System.Collections.Specialized;
using System.Linq;

namespace Exporter
{
    /// <summary>
    /// Class for reading application settings out of the app.config file
    /// </summary>
    public class AppSetting
    {
        private readonly NameValueCollection _setting = null;

        /// <summary>
        /// Gets or sets the server Url
        /// </summary>
        public string ServerUrl { get; private set; }

        /// <summary>
        /// Gets or sets the server port
        /// </summary>
        public string ServerPort { get; private set; }

        /// <summary>
        /// Gets or sets the published object via .NET Remoting
        /// </summary>
        public string RemotePublishedObject { get; private set; }

        /// <summary>
        /// Create an instance of <see cref="AppSetting"/> class
        /// </summary>
        /// <param name="setting">A collection of application setting</param>
        public AppSetting(NameValueCollection setting)
        {
            _setting = setting;
            ReadSettings();
        }

        private void ReadSettings()
        {
            if (_setting == null)
                return;

            // Remoting connection
            ServerUrl = ReadStringValue("ServerURL", "127.0.0.1");
            ServerPort = ReadStringValue("ServerPort", "8072");
            RemotePublishedObject = ReadStringValue("RemotePublishedObject", "GenericServer");
        }

        private string ReadStringValue(string appKey, string defaultValue)
        {
            if (_setting.AllKeys.Contains(appKey))
            {
                if (_setting[appKey] != null)
                    return _setting[appKey];
            }
            return defaultValue;
        }
    }
}
