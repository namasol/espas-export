﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using DataDomain.ApplicationDefinition;
using DataDomain.DataObjects;
using DataDomain.Utils;
using Exporter.Helpers;

namespace Exporter
{
    /// <summary>
    /// Class is for exporting person data to a xml file.
    /// </summary>
    public class PersonDataExport : DataExportBase
    {
        private string _outputLocation;
        private readonly int _numberOfConcurrentThread = 1;
        private IList<string> _wellKnownFields;

        private IDictionary<string, IList<AbstractMappedItem>> _categoryToFields;

        private readonly IDictionary<string, IList<string>> _gridsDefinition = new Dictionary<string, IList<string>>();
        private readonly IDictionary<string, IList<string>> _addressBoxesDefinition = new Dictionary<string, IList<string>>();

        private IDictionary<string, PersonData> _persons;
        public IDictionary<string, PersonData> Persons
        {
            get
            {
                if (_persons == null || _persons.Count == 0)
                    _persons = PromovaHelper.GetPersons(Dispatcher, ClientAppState);

                return _persons;
            }
        }

        public override string OutputLocation
        {
            get { return _outputLocation; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    var path = Path.Combine(Environment.CurrentDirectory, "PI");
                    _outputLocation = path;
                }
                else
                {
                    _outputLocation = value;
                }

                if (!Directory.Exists(_outputLocation))
                {
                    Directory.CreateDirectory(_outputLocation);
                }
            }
        }

        /// <summary>
        /// Create a <see cref="PersonDataExport"/> instance
        /// </summary>
        /// <param name="dispatcher">The dispatcher which contains proxies to work with PROmova server</param>
        /// <param name="clientAppState">The client application state</param>
        public PersonDataExport(ServerDispatcher dispatcher, ClientAppState clientAppState)
            : this(dispatcher, clientAppState, 1)
        {
        }

        /// <summary>
        /// Create a <see cref="PersonDataExport"/> instance
        /// </summary>
        /// <param name="dispatcher">The dispatcher which contains proxies to work with PROmova server</param>
        /// <param name="clientAppState">The client application state</param>
        /// <param name="numberOfConcurrentThread">The number of thread for exporting person data executing concurrently</param>
        public PersonDataExport(ServerDispatcher dispatcher, ClientAppState clientAppState, int numberOfConcurrentThread)
            : base(dispatcher, clientAppState)
        {
            _numberOfConcurrentThread = numberOfConcurrentThread;
        }

        /// <summary>
        /// 
        /// </summary>
        private void ReadPersonDefinition()
        {
            _wellKnownFields = new List<string>
            {
                "Id",
                "FirstName",
                "LastName",
                "Organisation",
                "Unterorganization",
                "CategoryId",
                "Birthday",
                "CreatedDate",
                "Defunct",
                "Deleted"
            };
            _categoryToFields = new Dictionary<string, IList<AbstractMappedItem>>();

            var appDefinition = Dispatcher.ServerLogic.GetApplicationDefinitionData();
            var personInformationAppItem = new PersonInformationApplicationItem();
            appDefinition.ReadSettings(ref personInformationAppItem, personInformationAppItem.Name);

            GenericApplicationData genericApplicationData = appDefinition.ApplicationsTemplate["Person Information"];
            personInformationAppItem.ReadSettings(genericApplicationData.Settings, appDefinition.GeneralSettings);

            if (personInformationAppItem.TabFieldCategories == null)
                return;

            foreach (TabFieldCategory tabCategory in personInformationAppItem.TabFieldCategories)
            {
                var categoryId = tabCategory.CategoryID;
                var mappedItems = new List<AbstractMappedItem>();

                if (tabCategory.TabPages == null)
                    continue;

                foreach (TabPage tabPage in tabCategory.TabPages)
                {
                    if (tabPage.Fields == null)
                        continue;

                    foreach (var field in tabPage.Fields)
                    {
                        // Mapped item for the field
                        if (!mappedItems.Contains(field))
                        {
                            mappedItems.Add(field);
                        }

                        // Get the field name and its definition
                        if (field.AbstractDataField is GridFieldDefinition)
                        {
                            var gridDef = field.AbstractDataField as GridFieldDefinition;
                            if (gridDef.Columns == null || gridDef.Columns.Count <= 0)
                                continue;

                            if (_gridsDefinition.ContainsKey(gridDef.Name))
                                continue;

                            _gridsDefinition[gridDef.Name] = gridDef.Columns.OfType<FieldGridColumn>().Select(x => x.Name).ToList();
                            continue;
                        }

                        if (field.AbstractDataField is AddressBoxFieldDefinition)
                        {
                            var addressBoxDef = field.AbstractDataField as AddressBoxFieldDefinition;
                            if (addressBoxDef.AddressFieldsDefinition == null)
                                continue;

                            if (_addressBoxesDefinition.ContainsKey(addressBoxDef.Name))
                                continue;

                            _addressBoxesDefinition[addressBoxDef.Name] = addressBoxDef.AddressFieldsDefinition.Fields.Select(x => x.Name).ToList();
                        }
                    } // foreach (var field in tabPage.Fields)
                }

                _categoryToFields[categoryId] = mappedItems;
            }
        }

        /// <summary>
        /// Creates xml file path
        /// </summary>
        /// <param name="xmlFileName">The file name</param>
        /// <returns></returns>
        private string GetXmlFilePath(string xmlFileName)
        {
            return Path.Combine(OutputLocation, xmlFileName);
        }

        /// <summary>
        /// Creates xml document including root node
        /// </summary>
        /// <param name="rootName">The root node name</param>
        /// <returns></returns>
        private XmlDocument CreateXmlDocument(string rootName)
        {
            var doc = new XmlDocument();
            doc.CreateDeclaration();
            doc.CreateChildElement(rootName);
            return doc;
        }

        /// <summary>
        /// Creates xml node for a person data including childen node for detailed data
        /// </summary>
        /// <param name="doc">The xml document</param>
        /// <param name="personData">The person data</param>
        private void AddPersonDataNode(XmlDocument doc, PersonData personData)
        {
            var rootNode = doc.SelectSingleNode("//Persons");
            if (rootNode == null)
            {
                throw new InvalidOperationException("The root node 'Persons' is not found.");
            }

            if (!_categoryToFields.ContainsKey(personData.CategoryID))
            {
                throw new InvalidOperationException(string.Format("Person has Category Id = '{0}' which is not found.", personData.CategoryID));
            }

            var personNode = doc.CreateElement("Person");
            personNode.CreateChildElement(doc, "Id", personData.ID);
            personNode.CreateChildElement(doc, "FirstName", personData.FirstName);
            personNode.CreateChildElement(doc, "LastName", personData.LastName);
            personNode.CreateChildElement(doc, "Organisation", personData.Oganization);
            personNode.CreateChildElement(doc, "Unterorganization", personData.Unterorganisation);
            personNode.CreateChildElement(doc, "CategoryId", personData.CategoryID);
            personNode.CreateChildElement(doc, "Birthday", personData.BirthDate);
            personNode.CreateChildElement(doc, "CreatedDate", personData.CreateDate.ToString("dd.MM.yyyy HH:mm"));
            personNode.CreateChildElement(doc, "Defunct", personData.Defunct ? "true" : "false");

            var fields = _categoryToFields[personData.CategoryID];
            var fieldsData = Dispatcher.PersonBusinessLogic.GetFieldsData(personData.ID, fields.ToArray(), DateTime.UtcNow);

            if (fieldsData != null)
            {
                foreach (DictionaryEntry entry in fieldsData)
                {
                    var fieldName = (string)entry.Key;
                    if (_wellKnownFields.Contains(fieldName))
                        continue;

                    var fieldData = entry.Value as AbstractGenericData;
                    // Grid data field
                    if (_gridsDefinition.ContainsKey(fieldName))
                    {
                        var fieldNode = personNode.CreateChildElement(doc, fieldName.EscapseSpecialCharacters());
                        CreateXmlNodesForGridField(doc, fieldNode, fieldData);
                    }
                    else if (_addressBoxesDefinition.ContainsKey(fieldName))
                    {
                        var fieldNode = personNode.CreateChildElement(doc, fieldName.EscapseSpecialCharacters());
                        CreateXmlNodesForAddressField(doc, fieldNode, fieldData);
                    }
                    // Normal data field
                    else
                    {
                        personNode.CreateChildElement(doc, fieldName.EscapseSpecialCharacters(), fieldData != null && fieldData.Value != null ? fieldData.Value.ToString() : "");
                    }
                }
            }

            rootNode.AppendChild(personNode);
        }

        /// <summary>
        /// Creates nodes for the case of person data field is address type
        /// </summary>
        /// <param name="doc">The xml document</param>
        /// <param name="fieldNode">The xml node for the address field</param>
        /// <param name="fieldData">The field data</param>
        private void CreateXmlNodesForAddressField(XmlDocument doc, XmlNode fieldNode, AbstractGenericData fieldData)
        {
            var addressData = fieldData as AddressInformation;
            if (addressData != null && addressData.AddressFields != null)
            {
                foreach (DictionaryEntry entry in addressData.AddressFields)
                {
                    var elementFieldName = entry.Key as string;
                    var elementFieldValue = entry.Value as AbstractGenericData;

                    if (string.IsNullOrWhiteSpace(elementFieldName))
                        continue;

                    fieldNode.CreateChildElement(doc,
                        elementFieldName.EscapseSpecialCharacters(),
                        elementFieldValue != null && elementFieldValue.Value != null ? elementFieldValue.Value.ToString() : "");
                }
            }
        }

        /// <summary>
        /// Creates nodes for the case of person data field is grid type
        /// </summary>
        /// <param name="doc">The xml document</param>
        /// <param name="fieldNode">The xml node for the grid field</param>
        /// <param name="fieldData">The filed data</param>
        private void CreateXmlNodesForGridField(XmlDocument doc, XmlNode fieldNode, AbstractGenericData fieldData)
        {
            var gridData = fieldData as GridCompostiteGenericData;
            if (gridData != null && gridData.Value != null)
            {
                IDictionary<string, int> columnIndexes = new Dictionary<string, int>();
                for (int index = 0; index < gridData.ColumnsOrder.Count; index++)
                {
                    columnIndexes[gridData.ColumnsOrder[index]] = index;
                }

                var gridDataArr = gridData.Value as ArrayList;
                if (gridDataArr != null)
                {
                    foreach (var dataItem in gridDataArr)
                    {
                        var dataRow = dataItem as CompostiteGenericData;
                        if (dataRow == null)
                            continue;

                        var rowNode = doc.CreateElement("Row");
                        fieldNode.AppendChild(rowNode);

                        var dataRowArr = dataRow.Value as ArrayList;
                        if (dataRowArr != null)
                        {
                            foreach (string columnName in _gridsDefinition[fieldNode.Name])
                            {
                                var indexOfColumn = columnIndexes.ContainsKey(columnName)
                                    ? columnIndexes[columnName]
                                    : -1;
                                if (indexOfColumn == -1)
                                    continue;

                                var dataRowItem = dataRowArr[indexOfColumn] as AbstractGenericData;
                                rowNode.CreateChildElement(doc,
                                    columnName.Replace(" ", "").Replace(@"/", ""),
                                    dataRowItem != null && dataRowItem.Value != null ? dataRowItem.Value.ToString() : "");
                            }
                        }
                    }
                }
            }
        }

        private int _totalPersons;
        private int _doneCounter = 0;

        private void IncreaseDoneCounter()
        {
            Interlocked.Increment(ref _doneCounter);
        }

        /// <summary>
        /// Writes a collection of person data into a xml file
        /// </summary>
        /// <param name="persons"></param>
        /// <returns></returns>
        private string WritePersonsDataToXml(PersonDataCollection persons)
        {
            var xmlFile = GetXmlFilePath(string.Format("Persons_{0}.xml", Guid.NewGuid()));
            var doc = CreateXmlDocument("Persons");

            if (persons != null)
            {
                foreach (DictionaryEntry person in persons)
                {
                    var personData = (PersonData)person.Value;
                    IncreaseDoneCounter();
                    OnPersonExported(new PersonExportEventArgs
                    {
                        Indexer = _doneCounter,
                        Total = _totalPersons,
                        PersonId = personData.ID,
                        PersonName = personData.FullName
                    });

                    AddPersonDataNode(doc, personData);
                }
            }
            doc.Save(xmlFile);
            return xmlFile;
        }

        /// <summary>
        /// Merges seperated xml files into one
        /// </summary>
        /// <param name="xmlPartFiles">A list of xml files to be merged</param>
        /// <returns>Returns the path the final xml file</returns>
        private string MergeXmlFiles(List<string> xmlPartFiles)
        {
            if (xmlPartFiles == null || xmlPartFiles.Count == 0)
                return null;

            var finalXmlFile = GetXmlFilePath("Persons.xml");
            if (File.Exists(finalXmlFile))
            {
                var renamed = GetXmlFilePath(string.Format("{0:yyyyMMddHHmm}_Persons.xml", DateTime.Now));
                File.Copy(finalXmlFile, renamed, true);
                File.Delete(finalXmlFile);
            }

            var finalDoc = CreateXmlDocument("Persons");
            var rootNode = finalDoc.SelectSingleNode("//Persons");

            var xml = new StringBuilder();
            foreach (string xmlFile in xmlPartFiles)
            {
                var doc = new XmlDocument();
                doc.Load(xmlFile);

                var rNode = doc.SelectSingleNode("//Persons");
                var xmlContent = rNode != null ? rNode.InnerXml : "";
                xml.Append(xmlContent);
            }
            rootNode.InnerXml = xml.ToString();
            finalDoc.Save(finalXmlFile);

            return finalXmlFile;
        }

        /// <summary>
        /// Deletes the xml files
        /// </summary>
        /// <param name="xmlPartFiles">A list of xml files to be deleted</param>
        private void TryToDeleteFiles(IEnumerable<string> xmlPartFiles)
        {
            try
            {
                foreach (string xmlFile in xmlPartFiles)
                {
                    File.Delete(xmlFile);
                }
            }
            catch { }
        }

        /// <summary>
        /// Exports persons data to a xml file.
        /// </summary>
        public override void DoExport()
        {
            _totalPersons = Persons.Count;
            OnNotify(string.Format("There are {0} person to be exporting", _totalPersons));

            OnNotify("Reading person information definition...");
            ReadPersonDefinition();

            Thread.Sleep(100);
            // Split the collection into multiparts
            int numberOfPart = _numberOfConcurrentThread > 0 ? _numberOfConcurrentThread : 1;
            var collections = new PersonDataCollection[numberOfPart];
            var xmlFiles = new List<string>();

            int indexer = 0;
            foreach (var entry in Persons)
            {
                if (collections[indexer % numberOfPart] == null)
                    collections[indexer % numberOfPart] = new PersonDataCollection();

                collections[indexer % numberOfPart].Add(entry.Key, entry.Value);
                indexer++;
            }

            var tasks = collections.Select(pc => Task.Factory.StartNew(() =>
            {
                // Write the person collection to a temporary xml file
                return WritePersonsDataToXml(pc);
            })
            .ContinueWith(output =>
            {
                // Put the file path to the array
                xmlFiles.Add(output.Result);
            })).ToArray();

            Task.WaitAll(tasks);

            MergeXmlFiles(xmlFiles);
            TryToDeleteFiles(xmlFiles);
        }
    }
}