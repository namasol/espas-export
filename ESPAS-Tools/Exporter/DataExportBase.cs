﻿using System;
using DataDomain.Utils;

namespace Exporter
{
    /// <summary>
    /// 
    /// </summary>
    public class NotifyEventArgs : EventArgs
    {
        public string Message { get; set; }
    }

    /// <summary>
    /// Argument for person export data event
    /// </summary>
    public class PersonExportEventArgs : EventArgs
    {
        public string PersonName { get; set; }
        public string PersonId { get; set; }

        public int Total { get; set; }
        public int Indexer { get; set; }
    }

    public abstract class DataExportBase
    {

        protected ClientAppState ClientAppState;
        protected ServerDispatcher Dispatcher;

        public event EventHandler<NotifyEventArgs> Notify;
        public event EventHandler<PersonExportEventArgs> PersonExported;


        protected DataExportBase(ServerDispatcher dispatcher, ClientAppState clientAppState)
        {
            this.Dispatcher = dispatcher;
            this.ClientAppState = clientAppState;
        }

        public virtual void OnNotify(string message)
        {
            var handler = Notify;
            if (handler != null)
            {
                handler(this, new NotifyEventArgs { Message = message });
            }
        }

        /// <summary>
        /// Raise up the PersonExported event
        /// </summary>
        /// <param name="args"></param>
        public virtual void OnPersonExported(PersonExportEventArgs args)
        {
            var handler = PersonExported;
            if (handler != null)
            {
                handler(this, args);
            }
        }

        public abstract string OutputLocation { get; set; }
        public abstract void DoExport();
    }
}
