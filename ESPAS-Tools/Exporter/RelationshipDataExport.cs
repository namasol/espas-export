﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using DataDomain.DataObjects;
using DataDomain.Utils;
using Exporter.Helpers;

namespace Exporter
{
    /// <summary>
    /// Class is for exporting relationship data to a XML file.
    /// </summary>
    public class RelationshipDataExport : DataExportBase
    {
        private string _outputLocation;
        private IDictionary<string, GenericPerson> _clients;

        public IDictionary<string, GenericPerson> Clients
        {
            get
            {
                if (_clients == null || _clients.Count == 0)
                    _clients = GetClients();

                return _clients;
            }
            set { _clients = value; }
        }

        /// <summary>
        /// Gets or sets the location that the relationship data be output to.
        /// </summary>
        public override string OutputLocation
        {
            get { return _outputLocation; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    var path = Path.Combine(Environment.CurrentDirectory, "Relationship");
                    _outputLocation = path;
                }
                else
                {
                    _outputLocation = value;
                }

                if (!Directory.Exists(_outputLocation))
                {
                    Directory.CreateDirectory(_outputLocation);
                }
            }
        }

        /// <summary>
        /// Creates an instance of <see cref="RelationshipDataExport"/>.
        /// </summary>
        /// <param name="dispatcher">The dispatcher which consist of proxies to the PROmova server.</param>
        /// <param name="clientAppState">The client state.</param>
        public RelationshipDataExport(ServerDispatcher dispatcher, ClientAppState clientAppState)
            : base(dispatcher, clientAppState)
        {
        }


        public override void DoExport()
        {
            var xmlFile = GetXmlFilePath("Relationships.xml");
            if (File.Exists(xmlFile))
            {
                var renamed = GetXmlFilePath(string.Format("{0:yyyyMMddHHmm}_Persons.xml", DateTime.Now));
                File.Copy(xmlFile, renamed, true);
                File.Delete(xmlFile);
            }

            var doc = CreateXmlDocument("Relations");

            // Gets current date
            var currentDate = Dispatcher.ServerLogic.GetCurrentDate();

            int counter = 0;
            var totalClients = Clients.Count;

            foreach (var pair in Clients)
            {
                var clientId = pair.Key;
                var diagramData = Dispatcher.PersonBusinessLogic.GetPersonDiagramData(ClientAppState, clientId, currentDate);
                if (diagramData != null)
                {
                    AddRelationshipsNode(doc,clientId, diagramData);
                    counter++;
                    OnPersonExported(new PersonExportEventArgs
                    {
                        Indexer = counter,
                        Total = totalClients,
                        PersonId = pair.Key,
                        PersonName = pair.Value.FullName
                    });
                }
            }

            doc.Save(xmlFile);
        }

        private void AddRelationshipsNode(XmlDocument doc, string personId, DiagramData diagramData)
        {
            if (doc == null || diagramData == null)
                return;

            var rootNode = doc.SelectSingleNode("//Relations");
            if (rootNode == null)
            {
                throw new InvalidOperationException("The root node 'Relations' is not found.");
            }

            var relationshipsNode = doc.CreateElement("Relationships");
            relationshipsNode.AddAttribute(doc, "PersonId", personId);

            if (diagramData.DiagramRelations != null)
            {
                for (int index = 0; index < diagramData.DiagramRelations.Count; index++)
                {
                    var relation = diagramData.DiagramRelations[index] as DiagramPersonRelationData;
                    if (relation == null)
                        continue;

                    var relationshipNode = relationshipsNode.CreateChildElement(doc, "Relationship");
                    relationshipNode.AddAttribute(doc, "PersonId", relation.FirstID == personId ? relation.SecondID : relation.FirstID);
                    bool concerningClientIsSecondOne = relation.SecondID == personId;

                    if (relation.RelationInfo != null && relation.RelationInfo.Count > 0)
                    {
                        var ri = relation.RelationInfo[relation.RelationInfo.Count - 1];
                        if (ri != null)
                        {
                            if (concerningClientIsSecondOne)
                            {
                                relationshipNode.AddAttribute(doc, "RoleOne", ri.SecondPersonRole ?? "");
                                relationshipNode.AddAttribute(doc, "RoleTwo", ri.FirstPersonRole ?? "");
                            }
                            else
                            {
                                relationshipNode.AddAttribute(doc, "RoleOne", ri.FirstPersonRole ?? "");
                                relationshipNode.AddAttribute(doc, "RoleTwo", ri.SecondPersonRole ?? "");
                            }
                        }
                        else
                        {
                            relationshipNode.AddAttribute(doc, "RoleOne", "");
                            relationshipNode.AddAttribute(doc, "RoleTwo", "");
                        }
                    }
                }
            }

            rootNode.AppendChild(relationshipsNode);
        }

        /// <summary>
        /// Creates xml file path
        /// </summary>
        /// <param name="xmlFileName">The file name</param>
        /// <returns></returns>
        private string GetXmlFilePath(string xmlFileName)
        {
            return Path.Combine(OutputLocation, xmlFileName);
        }

        /// <summary>
        /// Creates xml document including root node
        /// </summary>
        /// <param name="rootName">The root node name</param>
        /// <returns></returns>
        private XmlDocument CreateXmlDocument(string rootName)
        {
            var doc = new XmlDocument();
            doc.CreateDeclaration();
            doc.CreateChildElement(rootName);
            return doc;
        }

        /// <summary>
        /// Gets all client which are still active.
        /// </summary>
        /// <returns></returns>
        private IDictionary<string, GenericPerson> GetClients()
        {
            OnNotify("Getting active clients...");

            var persons = PromovaHelper.GetPersons(Dispatcher, ClientAppState);

            var clients = new Dictionary<string, GenericPerson>();
            foreach (var pair in persons)
            {
                string personId = pair.Key;
                var genericPerson = Dispatcher.PersonBusinessLogic.GetGenericPerson(personId);
                if ((genericPerson as IClient) != null && !clients.ContainsKey(personId))
                {
                    clients[personId] = genericPerson;
                }
            }

            return clients;
        }
    }
}